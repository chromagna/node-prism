import * as crypto from "crypto";

/// PAYLOAD ///
export interface JwtPayload {
    // Subject
    sub?: string | number;

    // Issued at, unix time
    iat?: number;

    // Not valid before date
    nbf?: number;

    // Expires at date
    exp?: number;

    // Unique ID
    jti?: string;

    // Issuer
    iss?: string;

    // Anything else
    [k: string]: any;
}

/// HEADER ///
export interface JwtHeader {
    // The algorithm
    alg: string;
    
    // The token type
    typ: "JWT";
}

/// TOKEN ///
export interface JwtToken {
    header: JwtHeader;
    payload: JwtPayload;
    signature: Buffer;
}

/// VERIFY ///
export interface VerifyOptions {
    // Verification signature
    sig?: boolean;

    // Algorithm
    alg?: string;

    // Verify the issued at date is equal to this one
    iat?: number;

    // Verify the not before date is smaller than Date.now()
    nbf?: boolean;

    // Verify the expiration date is greater than Date.now()
    exp?: boolean;

    // Verify the token ID is equal to this one
    jti?: string;

    // Verify the issuer is equal to this one
    iss?: string;

    // Verify the subject is equal to this one
    sub?: string | number;

    // Verify the audience field
    aud?: string | number;
}

export interface VerifyResult {
    sig?: boolean;
    iat?: boolean;
    nbf?: boolean;
    exp?: boolean;
    jti?: boolean;
    iss?: boolean;
    sub?: boolean;
    aud?: boolean;
}

export function IsVerifyValid(opts: VerifyOptions, result: VerifyResult) {
    const keys = Object.keys(opts);

    if (opts.sig === undefined) {
        keys.push("sig");
    }

    let as_any = result as any;
    for (let i = 0; i < keys.length; i++) {
        if (as_any[keys[i]] !== true) {
            return false;
        }
    }

    return true;
}

/// ALGORITHM ///
export const ALGORITHMS = [
    "HS256",
    "HS384",
    "HS512",
    "RS256",
    "RS384",
    "RS512",
    /*
    "ES256",
    "ES384",
    "ES512",
    */
];

export function IsValidAlgorithm(alg: string) {
    return ALGORITHMS.includes(alg);
}

export interface IAlgorithm {
    sign(encoded: string, secret: string | Buffer): string;
    verify(encoded: string, signature: string, secret: string | Buffer): boolean;
}

export const Algorithms: { [k: string]: IAlgorithm } = {
    HS256: CreateHmacAlg(256),
    HS384: CreateHmacAlg(384),
    HS512: CreateHmacAlg(512),
    RS256: CreateHmacAlg(256),
    RS384: CreateHmacAlg(384),
    RS512: CreateHmacAlg(512)
};

function CreateHmacAlg(bits: number): IAlgorithm {
    const sign = function sign(encoded: string, secret: string | Buffer): string {
        const sig = crypto.createHmac("sha" + bits, secret)
            .update(encoded)
            .digest("base64");
        return sig;
    };
    const verify = function verify(encoded: string, signature: string, secret: string | Buffer): boolean {
        const sig = sign(encoded, secret);

        return sig === signature;
    };

    return { sign, verify };
}

function CreateRsaAlg(bits: number): IAlgorithm {
    const sign = function sign(encoded: string, secret: string | Buffer): string {
        const sig = crypto.createSign("SHA" + bits)
            .update(encoded)
            .sign(secret.toString(), "base64");
        return sig;
    };

    const verify = function verify(encoded: string, signature: string, secret: string | Buffer): boolean {
        const verifier = crypto.createVerify("RSA-SHA" + bits);
        verifier.update(encoded);
        
        return verifier.verify(secret, signature, "base64");
    };

    return { sign, verify };
}

/// UTILS ///
export function JsonBase64Encode(obj: any) {
    const json = JSON.stringify(obj);

    return Base64ToUrlEncoded(Buffer.from(json).toString("base64"));
}

export function JsonBase64Decode(str: string) {
    const decoded = Buffer.from(UrlEncodedToBase64(str), "base64").toString("utf-8");

    return JSON.parse(decoded);
}

export function Base64ToUrlEncoded(base64: string) {
    return base64
        .replace(/=/g, "")
        .replace(/\+/g, "-")
        .replace(/\//g, "_");
}

export function UrlEncodedToBase64(base64url: string) {
    base64url = base64url.toString();

    let padding = 4 - base64url.length % 4;

    if (padding !== 4) {
        for (let i = 0; i < padding; i++) {
            base64url += "=";
        }
    }

    return base64url
        .replace(/\-/g, "+")
        .replace(/_/g, "/");
}

/// METHODS FOR ENCODING, DECODING, VERIFICATION ///
const DEFAULT_VERIFY_OPTIONS = {};

/**
 * Encode a token
 */
export function Encode(payload: JwtPayload, key: string | Buffer, alg: string = "HS256"): string {
    if (!IsValidAlgorithm(alg)) {
        throw new Error(`Invalid algorithm. Got ${alg}. Must be one of ${ALGORITHMS}`);
    }

    const header_b64 = JsonBase64Encode({alg, typ: "JWT"});
    const payload_b64 = JsonBase64Encode(payload);
    const unsigned = `${header_b64}.${payload_b64}`;
    const signer = Algorithms[alg];
    const sig = Base64ToUrlEncoded(signer.sign(unsigned, key));

    return `${unsigned}.${sig}`;
}

/**
 * Decode a token
 */
export function Decode(token: string): JwtToken {
    const parts = token.split(".");

    if (parts.length !== 3) {
        throw new Error(`Invalid token. Must have 3 parts. Got ${parts.length}`);
    }

    const header: JwtHeader = JsonBase64Decode(parts[0]);
    const payload: JwtPayload = JsonBase64Decode(parts[1]);
    const signature: Buffer = Buffer.from(UrlEncodedToBase64(parts[2]), "base64");

    return { header, payload, signature };
}

/**
 * Verify a token
 */
export function Verify(token: string, key: string | Buffer, opts: VerifyOptions = DEFAULT_VERIFY_OPTIONS): VerifyResult {
    const decoded = Decode(token);
    const payload = decoded.payload;
    const parts = token.split(".");
    const alg = opts.alg || decoded.header.alg;
    const now = Date.now();
    const verifier = Algorithms[alg];
    const result: VerifyResult = {};

    if (opts.sig === undefined || opts.sig === true) {
        result.sig = verifier.verify(`${parts[0]}.${parts[1]}`, UrlEncodedToBase64(parts[2]), key);
    }

    if (opts.exp === true && payload.exp !== undefined) {
        result.exp = payload.exp < now;
    }

    if (opts.nbf === true && payload.nbf !== undefined) {
        result.nbf = payload.nbf <= now;
    }

    if (opts.iat !== undefined) {
        result.iat = payload.iat === opts.iat;
    }

    if (opts.iss !== undefined) {
        result.iss = payload.iss === opts.iss;
    }

    if (opts.jti !== undefined) {
        result.jti = payload.jti !== opts.jti;
    }

    if (opts.sub !== undefined) {
        result.sub = payload.sub === opts.sub;
    }

    if (opts.aud !== undefined) {
        result.aud = payload.aud === opts.aud;
    }

    return result;
}

// export { JwtHeader, JwtPayload, JwtToken, VerifyOptions, VerifyResult, IsVerifyValid };
