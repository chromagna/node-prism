export interface SerializedError {
    name: string;
    message: string;
    stack: string;
    [prop: string]: any;
}

export default class ErrorSerializer {
    static serialize(error: Error): SerializedError {
        const data = {
            message: error.message,
            name: error.name,
            stack: error.stack,
        };

        Object.assign(data, error);
        
        return data as SerializedError;
    }

    static deserialize(data: SerializedError) {
        const factory = this.getFactory(data);
        const error = new factory(data.message);

        Object.assign(error, data);

        return error;
    }

    private static getFactory(data: SerializedError): new (message: string) => Error {
        const name = data.name;

        if (name.endsWith("Error") && global[name]) return global[name];

        return Error;
    }
}