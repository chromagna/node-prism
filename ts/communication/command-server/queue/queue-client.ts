import { TokenClient, TokenClientOptions } from "../../token-server/token/client";
import Queue from "./queue";
import QueueItem from "./queue-item";

export default class QueueClient extends TokenClient {
    private queue = new Queue<Buffer>();

    constructor(options: TokenClientOptions) {
        super(options);
        this.applyEvents();
    }

    sendToken(token: Buffer, expiresIn: number): void {
        const success = this.send(token);
        if (!success) this.queue.add(token, expiresIn);
    }

    private applyEvents() {
        this.on("connect", () => {
            while (!this.queue.isEmpty) {
                const item = this.queue.pop() as QueueItem<Buffer>;
                this.sendToken(item.value, item.expiresIn);
            }
        });
    }
}