export default class QueueItem<T> {
    value: T;
    private expireTime: number;

    constructor(value: T, expiresIn: number) {
        this.value = value;
        this.expireTime = Date.now() + expiresIn;
    }

    get expiresIn(): number {
        return this.expireTime - Date.now();
    }

    get isExpired(): boolean {
        return Date.now() > this.expireTime;
    }
}