import QueueItem from "./queue-item";

export default class Queue<T> {
    private items: Array<QueueItem<T>> = [];
    
    add(item: T, expiresIn: number): void {
        this.items.push(new QueueItem(item, expiresIn));
    }

    get isEmpty(): boolean {
        let i = this.items.length;

        while (i--) {
            const item = this.items[i];

            if (item.isExpired) this.items.splice(i, 1);
            else return false;
        }

        return true;
    }

    pop(): QueueItem<T> | null {
        while (this.items.length) {
            const item = this.items.shift() as QueueItem<T>;

            if (!item.isExpired) return item;
        }

        return null;
    }
}