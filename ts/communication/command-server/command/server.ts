import { Connection, TokenServer, TokenServerOptions } from "../../token-server";
import CodeError from "../error/code-error";
import ErrorSerializer from "../error/error-serializer";
import { Command } from "./command";

export type CommandImplementation = (payload: any, connection: Connection) => Promise<any>;

export class CommandServer extends TokenServer {
    private commands: { [command: number]: CommandImplementation } = {};

    constructor(options: TokenServerOptions) {
        super(options);

        this.initEvents();
    }

    command(command: number, fn: CommandImplementation): void {
        this.commands[command] = fn;
    }

    private initEvents() {
        this.on("token", async (token, connection) => {
            try {
                const { id, command, payload } = Command.parse(token);
                this.runCommand(id, command, payload, connection);
            } catch (error) {
                this.emit("error", error);
            }
        });
    }

    private async runCommand(id: number, command: number, payload: any, connection: Connection): Promise<void> {
        try {
            if (!this.commands[command]) throw new CodeError(`Command [${command}] not found.`, "ENOTFOUND", "CommandError");

            const result = await this.commands[command](payload, connection);

            connection.send(Command.tokenize({
                command,
                id,
                payload: result,
            }));
        } catch (error) {
            const payload = ErrorSerializer.serialize(error);

            connection.send(Command.tokenize({
                command: 255,
                id,
                payload,
            }));
        }
    }
}