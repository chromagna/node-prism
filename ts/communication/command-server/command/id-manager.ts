export default class IdManager {
    ids: Array<true | false> = [];
    private index: number = 0;
    private maxIndex: number;

    constructor(maxIndex: number) {
        this.maxIndex = maxIndex;
    }

    release(id: number) {
        if (id < 0 || id > this.maxIndex) throw new TypeError(`ID must be between 0 and ${this.maxIndex}. Received ${id}.`);
        this.ids[id] = false;
    }

    reserve(): number {
        const startIndex = this.index;

        while(true) {
            const i = this.index;

            if (!this.ids[i]) {
                this.ids[i] = true;
                return i;
            }

            if (this.index >= this.maxIndex) this.index = 0;
            else this.index++;

            if (this.index === startIndex) throw new Error(`All IDs are reserved. Make sure to release IDs when they are no longer used.`);
        }
    }
}