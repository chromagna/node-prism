import { EventEmitter } from "events";
import { Duplex } from "stream";
import { Message, NEWLINE } from "./message";

const CLOSE_TOKEN = Buffer.from("\\\n");

export declare interface Connection extends EventEmitter {
    on(event: "token", handler: (token: Buffer) => void): this;
    on(event: "remoteClose", handler: () => void): this;
    on(event: "close", handler: () => void): this;
    
    once(event: "token", handler: (token: Buffer) => void): this;
    once(event: "remoteClose", handler: () => void): this;
    once(event: "close", handler: () => void): this;

    emit(event: "token", token: Buffer): boolean;
    emit(event: "remoteClose"): boolean;
    emit(event: "close"): boolean;
}

export class Connection extends EventEmitter {
    socket: Duplex;
    private buffer = Buffer.allocUnsafe(0);

    constructor(socket: Duplex) {
        super();
        this.socket = socket;
        this.applyEvents();
    }

    get isDead(): boolean {
        return !this.socket.writable;
    }

    send(data: Buffer): boolean {
        if (this.isDead) return false;
        this.socket.write(Message.escape(data));
        return true;
    }

    close(): boolean {
        if (this.isDead) return false;
        this.socket.end();
        return true;
    }

    remoteClose(): boolean {
        if (this.isDead) return false;
        this.socket.write(CLOSE_TOKEN);
        return true;
    }

    private applyEvents() {
        this.socket.on("data", (data: Buffer) => {
            this.buffer = Buffer.concat([this.buffer, data]);
            this.parse();
        });

        this.socket.on("close", () => this.emit("close"));
    }

    private parse(): void {
        while (true) {
            const i = this.buffer.indexOf(NEWLINE);

            if (i === -1) break;

            const data = this.buffer.slice(0, i + 1); // + 1 to include separating newline

            if (data.equals(CLOSE_TOKEN)) this.emit("remoteClose");
            else this.emit("token", Message.unescape(data));

            this.buffer = this.buffer.slice(i + 1);
        }
    }
}