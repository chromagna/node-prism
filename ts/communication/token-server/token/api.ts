import { EventEmitter } from "events";
import { Status } from "./status";
import { Connection } from "../connection/connection";

export type ErrorListener = (error: Error) => void;
export type ConnectListener = () => void;
export type CloseListener = (hadError: boolean) => void;
export type TokenListener = (token: Buffer, connection: Connection) => void;

export declare interface TokenAPI extends EventEmitter {
    on(event: "error", listener: ErrorListener): this;
    on(event: "connect", listener: ConnectListener): this;
    on(event: "close", listener: CloseListener): this;
    on(event: "token", listener: TokenListener): this;

    once(event: "error", listener: ErrorListener): this;
    once(event: "connect", listener: ConnectListener): this;
    once(event: "close", listener: CloseListener): this;
    once(event: "token", listener: TokenListener): this;

    emit(event: "error", Error): boolean;
    emit(event: "connect"): boolean;
    emit(event: "close", hadError: boolean): boolean;
    emit(event: "token", token: Buffer, connection: Connection): boolean;
}

export abstract class TokenAPI extends EventEmitter {
    status: Status;
    abstract close(): boolean;
    abstract connect(delay: number): boolean;
}