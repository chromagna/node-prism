import * as net from "net";
import * as tls from "tls";
import * as http from "http";
import { Connection } from "../connection/connection";
import { TokenAPI } from "./api";
import { Status } from "./status";

export interface SecurityOpts {
    useInsecure?: boolean;
    keepAlive?: boolean;
    keepAliveInitialDelay?: number;
}

export type TokenServerOptions = tls.TlsOptions & net.ListenOptions & net.SocketConstructorOpts & SecurityOpts;

export class TokenServer extends TokenAPI {
    connections: Array<Connection> = [];

    private options: TokenServerOptions;
    private server: tls.Server | net.Server;
    // private server: net.Server;
    private hadError: boolean;

    constructor(options: TokenServerOptions) {
        super();

        this.options = options;

        const self = this;

        if(this.options.useInsecure) {
            this.server = net.createServer(this.options, function(socket) {
                self.options.keepAlive && socket.setKeepAlive(true, self.options.keepAliveInitialDelay ?? 60000);
                socket.on("error", err => this.emit("clientError", err));
            });
        } else {
            this.server = tls.createServer(this.options, function(socket) {
                self.options.keepAlive && socket.setKeepAlive(true, self.options.keepAliveInitialDelay ?? 60000);
                socket.on("error", err => this.emit("clientError", err));
            });
        }

        this.applyListeners();
        this.connect();
    }

    // TODO: When client emits close - does this actually get received?
    close() {
        if (!this.server.listening) return false;

        this.status = Status.CLOSED;
        this.server.close();

        for (const connection of this.connections) connection.remoteClose();

        return true;
    }

    connect() {
        if (this.status >= Status.CONNECTING) return false;

        this.hadError = false;
        this.status = Status.CONNECTING;
        this.server.listen(this.options);

        return true;
    }

    private applyListeners() {
        this.server.on("listening", () => {
            this.status = Status.ONLINE;
            this.emit("connect");
        });

        this.server.on("tlsClientError", (error) => {});
        this.server.on("clientError", (error) => {});

        this.server.on("error", (error: Error) => {
            this.hadError = true;
            this.emit("error", error);
            this.server.close();
        });

        this.server.on("close", () => {
            this.status = Status.OFFLINE;
            this.emit("close", this.hadError);
        });

        this.server.on(`${this.options.useInsecure ? "connection" : "secureConnection"}`, (socket) => {
            const connection = new Connection(socket);
            this.connections.push(connection);

            connection.once("close", () => {
                const i = this.connections.indexOf(connection);
                this.connections.splice(i, 1);
            });

            connection.on("token", (token) => {
                this.emit("token", token, connection);
            });
        });
    }
}