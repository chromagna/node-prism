import * as net from "net";
import * as tls from "tls";
import { Connection } from "../connection/connection";
import { TokenAPI } from "./api";
import { SecurityOpts } from "./server";
import { Status } from "./status";

export type TokenClientOptions = tls.ConnectionOptions & net.SocketConstructorOpts & SecurityOpts;

export declare interface TokenClient extends TokenAPI {
    on(event: "remoteClose", listener: (connection: Connection) => void): this;
    on(event: string, listener: (...args: any[]) => void): this;
    
    once(event: "remoteClose", listener: (connection: Connection) => void): this;
    once(event: string, listener: (...args: any[]) => void): this;

    emit(event: "remoteClose", connection: Connection): boolean;
    emit(event: string, ...args: any[]): boolean;
}

export class TokenClient extends TokenAPI {
    private options: TokenClientOptions;
    // private socket: net.Socket;
    private socket: tls.TLSSocket | net.Socket;
    private connection: Connection | null = null;
    private hadError: boolean;

    constructor(options: TokenClientOptions) { 
        super();
        this.options = options;
        this.connect();
    }

    close() {
        if (this.status <= Status.CLOSED) return false;

        this.status = Status.CLOSED;
        this.socket.end();
        this.connection = null;

        return true;
    }

    connect() {
        if (this.status >= Status.CONNECTING) return false;

        this.hadError = false;
        this.status = Status.CONNECTING;

        if (this.options.useInsecure) this.socket = net.connect(this.options as any);
        else this.socket = tls.connect(this.options);

        this.connection = null;
        this.applyListeners();
        
        return true;
    }

    send(token: Buffer): boolean {
        if (this.connection) return this.connection.send(token);
        return false;
    }

    private applyListeners() {
        this.socket.on("error", (error: Error) => {
            this.hadError = true;
            this.emit("error", error);
        });

        this.socket.on("close", () => {
            this.status = Status.OFFLINE;
            this.emit("close", this.hadError);
        });

        this.socket.on(`${this.options.useInsecure ? "connect" : "secureConnect"}`, () => {
            this.updateConnection();
            this.status = Status.ONLINE;
            this.emit("connect");
        });
    }

    private updateConnection() {
        const connection = new Connection(this.socket);

        connection.on("token", (token) => {
            this.emit("token", token, connection);
        });

        connection.on("remoteClose", () => this.emit("remoteClose", connection));

        this.connection = connection;
    }
}