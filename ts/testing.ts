/**
 * This is a WIP.
 */
import * as Crypto from "crypto";
import * as Express from "express";

import * as C from "./controller";
import * as U from "./utils";

export let globalTestState: PrismTestsState;


/**
 * Registers a fixture of tests with the global test runner.
 * @param humanReadableName A human readable name.
 * @returns void
 */
export function Fixture(humanReadableName?: string): (f: Function) => void {
    return (ctr: Function) => {
        let fixture = globalTestState.getOrInsertFixture(ctr);
        fixture.humanReadableName = typeof (humanReadableName) === "string" ? humanReadableName : ctr.name;
        fixture.explicitlyDeclared = true;
    };
}

/**
 * Register a new test. If the test throws, it fails. Otherwise, it passes.
 * @param humanReadableName A human readable name.
 * @returns void
 */
export function Test(humanReadableName: string): (a: any, s: string, pd: PropertyDescriptor) => void {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        let f = globalTestState.getOrInsertFixture(target.constructor);
        let t = f.getOrInsertTest(propertyKey);
        t.humanReadableName = humanReadableName;
        t.explicitlyDeclared = true;
    };
}

/**
 * Method to run before any of the tests.
 * @returns void
 */
export function Before(): (a: any, s: string, pd: PropertyDescriptor) => void {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        globalTestState.getOrInsertFixture(target.constructor).runBeforeMethods.push(propertyKey);
    };
}

/**
 * Method to run after any of the tests.
 * @returns void
 */
export function After(): (a: any, s: string, pd: PropertyDescriptor) => void {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        globalTestState.getOrInsertFixture(target.constructor).runAfterMethods.push(propertyKey);
    };
}

export type PrismFixtureMap = {
    [key: string]: PrismFixture,
};

export type PrismTestMap = {
    [key: string]: PrismTest,
};

export class PrismTestsState {
    fixtures: PrismFixtureMap = {};

    getOrInsertFixture(ctr: Function): PrismFixture {
        let key = ctr.toString();

        if (this.fixtures[key] == undefined) {
            this.fixtures[key] = new PrismFixture();
            this.fixtures[key].ctr = ctr;
            this.fixtures[key].humanReadableName = this.fixtures[key].humanReadableName == undefined ? ctr.name : this.fixtures[key].humanReadableName;
        }

        return this.fixtures[key];
    }

    generateCompleteFixtureMetadata(): Object {
        let metadata = {};

        for (let fx in this.fixtures) {
            let fixture = this.fixtures[fx];
            let fxHash = PrismTestsState.getFixtureHashId(fixture);
            metadata[fxHash] = {};
            for (let t in fixture.tests) {
                metadata[fxHash][t] = true;
            }
        }

        return metadata;
    }

    static getFixtureHashId(fixture: PrismFixture): string {
        let hasher = Crypto.createHash("sha256");
        hasher.update(fixture.ctr.toString());

        return fixture.ctr.name + "_" + hasher.digest("hex").substr(0, 8);
    }

    generateRainbowTables(): Object {
        let ret = {};

        for (let fx in this.fixtures) {
            let fixture = this.fixtures[fx];
            let fxHash = PrismTestsState.getFixtureHashId(fixture);
            ret[fxHash] = fixture;
        }

        return ret;
    }

    async run(testsToRun?: Object): Promise<PrismTestResult[]> {

        if (testsToRun == undefined) {
            testsToRun = this.generateCompleteFixtureMetadata();
        }

        let testResults: PrismTestResult[] = [];
        let rainbowTables = this.generateRainbowTables();

        for (let fx in testsToRun) {
            let fixture: PrismFixture = rainbowTables[fx];
            let fi = Reflect.construct(fixture.ctr, []);

            for (let mn of fixture.runBeforeMethods) {
                let r = fi[mn]();
                if (r instanceof Promise) {
                    await r;
                }
            }

            for (let mn in testsToRun[fx]) {
                if (testsToRun[fx][mn] === true && fixture.tests[mn] != undefined && fi[mn] != undefined) {
                    let result = new PrismTestResult();
                    result.fixtureKey = fx;
                    result.fixtureDesc = fixture.humanReadableName;
                    result.testKey = mn;
                    result.testDesc = fixture.tests[mn].humanReadableName;

                    try {
                        let r = fi[mn]();
                        if (r instanceof Promise) {
                            await r;
                        }
                        result.passed = true;
                        result.message = "";
                    } catch (err) {
                        result.passed = false;
                        if (err instanceof Error) {
                            // result.message = JSON.stringify({ name: err.name, message: err.message, stack: err.stack });
                            result.message = { name: err.name, message: err.message, stack: err.stack };
                        } else if (err instanceof Object) {
                            result.message = err;
                        } else if (typeof (err) === "string") {
                            result.message = err;
                        } else {
                            result.message = err.toString();
                        }
                    }

                    testResults.push(result);
                }
            }

            for (let mn of fixture.runAfterMethods) {
                let r = fi[mn]();
                if (r instanceof Promise) {
                    await r;
                }
            }
        }

        return testResults;
    }
}

export class PrismFixture {
    runBeforeMethods: string[] = [];
    runAfterMethods: string[] = [];
    tests: PrismTestMap = {};
    ctr: Function;
    humanReadableName: string;
    explicitlyDeclared: boolean = false;

    getOrInsertTest(key: string): PrismTest {
        if (this.tests[key] == undefined) {
            this.tests[key] = new PrismTest();
        }

        return this.tests[key];
    }
}

export class PrismTest {
    humanReadableName: string;
    explicitlyDeclared: boolean = false;
}

export class PrismTestResult {
    fixtureDesc: string;
    fixtureKey: string;
    testDesc: string;
    testKey: string;
    passed: boolean;
    message: string | object;
}

globalTestState = new PrismTestsState();

async function runSetOfTestsAndFillResponse(testsToRun: Object, res: Express.Response): Promise<void> {
    let results = await globalTestState.run(testsToRun);
    let failed = false;
    for (let result of results) {
        if (result.passed === false) {
            failed = true;
        }
    }
    res.statusCode = failed ? 418 : 200;
    res.json(results);
}

let css = `body {
    font-family: Arial, sans-serif;
}
`;

function generateInteractiveTestRunnerMiddleware(useFixture?: PrismFixture): (req: Express.Request, res: Express.Response) => void {
    return (req: Express.Request, res: Express.Response) => {
        let content = `
        <html><head><style>${css}</style></head><body>`;

        let useFixtureHash: string = undefined;

        if (useFixture == undefined) {

        } else {
            useFixtureHash = PrismTestsState.getFixtureHashId(useFixture);
            content += `<div>Use fixture: ${useFixtureHash}</div>`;
        }

        for (let fx in globalTestState.fixtures) {
            let fixture = globalTestState.fixtures[fx];
            let fxHash = PrismTestsState.getFixtureHashId(fixture);

            if (useFixture != undefined && fxHash !== useFixtureHash) {
                continue;
            }

            content += `<div id="${fxHash}-header">`;
            content += `<input class="runner" id="${fxHash}" type="checkbox" checked />`;
            content += `<a href="fixture/${fxHash}/">${fixture.humanReadableName}</a>`;
            content += `</div>`;

            for (let t in fixture.tests) {
                let test = fixture.tests[t];
                content += `<div style="margin-left:20px;">`;
                content += `<input class="${fxHash}" id="${t}" type="checkbox" checked />`
                content += `${test.humanReadableName} <span id="${fxHash}_${t}_result"></span>`
                content += `</div>`;
            }
        }

        content += `</body></html>`;

        res.send(content);
    }
}

export function injectTestRunnerMiddleware(controller: C.PrismController) {
    controller.router.get("/all", async (req: Express.Request, res: Express.Response) => {
        await runSetOfTestsAndFillResponse(undefined, res);
    });

    controller.router.get("/metadata", (req: Express.Request, res: Express.Response) => {
        res.json(globalTestState.generateCompleteFixtureMetadata());
    });

    for (let fxk in globalTestState.fixtures) {
        let fixture = globalTestState.fixtures[fxk];
        let fxkHash = PrismTestsState.getFixtureHashId(fixture);
        controller.router.get(U.UrlJoin("/fixture/", fxkHash, "/"), generateInteractiveTestRunnerMiddleware(fixture));
        controller.router.get(U.UrlJoin("/fixture/", fxkHash, "/all"), async (req: Express.Request, res: Express.Response) => {
            let metadata = globalTestState.generateCompleteFixtureMetadata();
            let thisFixtureSet = {};
            for (let mk in metadata) {
                if (mk === fxkHash) {
                    thisFixtureSet[mk] = metadata[mk];
                }
            }
            await runSetOfTestsAndFillResponse(thisFixtureSet, res);
        });
    }

    controller.router.get("/", generateInteractiveTestRunnerMiddleware());
}