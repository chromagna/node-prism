import * as Express from "express";

interface GetTokenOptions {
	proxy?: boolean;
	header?: string;
}

export class Bearer {
	public static GetToken(req: Express.Request, options?: GetTokenOptions): string | undefined {
        let proxy;

        if (options) {
            proxy = options.proxy || null;
        }

		const auth = req.headers
			? proxy
				? req.headers["proxy-authorization"]
				: req.headers.authorization
			: null;

        if (auth) {
            const [_, token] = auth.split(" ");

            return token;
        } else {
            return undefined;
        }
	}
}
