import { PrismResponse } from "./";
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

export type ExpressErrorRequestHandler = (
	err: any,
	req: Express.Request,
	res: Express.Response,
	next: Function
) => void;
export let errorHandlers: ExpressErrorRequestHandler[] = [];

export let defaultErrorLogger = (toLog: any) => {
	console.error(toLog);
};
export let defaultWarnLogger = (toLog: any) => {
	console.warn(toLog);
};
export let defaultInfoLogger = (toLog: any) => {
	console.log(toLog);
};

export function UrlJoin(...parts: string[]): string {
	let ret = parts.join("/");

	// remove consecutive slashes
	ret = ret.replace(/([^\/]*)\/+/g, "$1/");

	// make sure protocol is followed by two slashes
	ret = ret.replace(/(:\/|:\/\/)/g, "://");

	// remove trailing slash before parameters or hash
	ret = ret.replace(/\/(\?|&|#[^!])/g, "$1");

	// replace ? in parameters with &
	ret = ret.replace(/(\?.+)\?/g, "$1&");

	return ret;
}

async function WrapRequest<T>(p: Promise<T>): Promise<[any, T | undefined]> {
	try {
		return [undefined, await p];
	} catch (err) {
		return [err, undefined];
	}
}

/**
 * 
 * @param url 
 * @param config AxiosRequestConfig
 * @returns Promise<Response<T>>
 */
function Get<T>(url: string, config: AxiosRequestConfig = undefined): Promise<PrismResponse<T>> {
	return new Promise((res, rej) => {
		axios
			.get<PrismResponse<T>>(url, config)
			.then((r: AxiosResponse<PrismResponse<T>>) => {
				if (r.data.error) rej(r.data);
				res(r.data);
			})
			.catch(err => rej(err));
	});
}

function Post<T>(url: string, postdata: any = undefined, config: AxiosRequestConfig = undefined): Promise<PrismResponse<T>> {
	return new Promise((res, rej) => {
		axios
			.post<PrismResponse<T>>(url, postdata, config)
			.then((r: AxiosResponse<PrismResponse<T>>) => {
				if (r.data.error) rej(r.data);
				res(r.data);
			})
			.catch(err => rej(err));
	});
}

export async function WrappedGet<T>(url: string, config: AxiosRequestConfig = undefined): Promise<[any, PrismResponse<T> | undefined]> {
	return WrapRequest<PrismResponse<T>>(Get<T>(url, config));
}

export async function WrappedPost<T>(url: string, postdata: any = undefined, config: AxiosRequestConfig = undefined): Promise<[any, PrismResponse<T> | undefined]> {
	return WrapRequest<PrismResponse<T>>(Post<T>(url, postdata, config));
}