import * as B from "busboy";
import * as Express from "express";
import * as FS from "fs";

import { Context } from "./controller";

export interface IUploadConfig {
    /**
     * Directory to upload the file to. Relative path.
     */
    dir?: string;
    /**
     * Scramble the filename? If not, use the one passed in the request.
     */
    scrambleFilename?: boolean;
    /**
     * What's the name of the field in the FormData that includes the file?
     */
    formFieldName?: string;
    /**
     * If true, the file buffer will be returned inside the FileRequest object from ProcessFileUploadRequest.
     */
    returnBuffer?: boolean;
}

/**
 * 
 * @param context Context
 * @param config 
 * @returns Throws an Error on failure, Promise of FileRequest if it succeeds.
 */
export function ProcessFileUploadRequest(context: Context, config?: IUploadConfig): Promise<FileRequest> {
    let request: Express.Request = context.request;
    let response: Express.Response = context.response;
    let b = new B({ "headers": request.headers });

    if (!config) config = {};
    config["dir"] = config["dir"] || "_uploads";
    config["scrambleFilename"] = config["scrambleFilename"] || false;
    config["formFieldName"] = config["formFieldName"] || "file";
    config["returnBuffer"] = config["returnBuffer"] || false;

    let servlet = new UploadServlet(config);

    return new Promise((resolve, reject) => {
        b.on("file", function (fieldname: string, file: any, filename: string, encoding: string, mimetype: string) {
            if (fieldname === config["formFieldName"]) {
                (request as any)["postFile"] = {
                    "filename": filename,
                    "data": null,
                };
                file.on("data", function (data: Buffer) {
                    const oldData: Buffer = (request as any)["postFile"]["data"];
                    const newData = oldData == null ? data : Buffer.concat([oldData, data]);
                    (request as any)["postFile"]["data"] = newData;
                });
            }
        });
        b.on("field", (fieldname: string, val: any, fieldnameTruncated: string, valTruncated: any, encoding: string, mimetype: string) => {
            if (fieldname === "data") {
                (request as any).postData = val;
            }
        });
        b.on("finish", async () => {
            let success = await servlet.doPost(request, response);
            if (success) {
                // Delete FileRequest.file Buffer? We don't need to return the file.
                if (servlet.req.file && !config["returnBuffer"]) delete servlet.req.file;
                return resolve(servlet.req);
            }
            throw new Error("Could not save file.");
        });
        b.on("error", (e) => {
            throw new Error(e);
        });
        request.pipe(b);
    });
}

export interface FileRequest {
    originalFileName?: string;
    fileName?: string;
    fileSize?: number;
    file?: Buffer;
    fullPath?: string;
}

class UploadServlet {

    protected conf: IUploadConfig;
    public req: FileRequest = null;

    constructor(conf: IUploadConfig) {
        this.conf = conf;
    }

    protected getReq(request: Express.Request, response: Express.Response): FileRequest {
        let dataJson: string = (request as any).postData;
        let fileName: string = null;
        let fileSize: number = -1;
        let file: Buffer;
        let orig: string = null;

        if ((request as any)["postFile"]) {
            file = (request as any)["postFile"]["data"];
            fileName = (request as any)["postFile"]["filename"];
            orig = fileName;
            fileSize = file.length
        }

        if (this.getParam("scrambleFilename", false, false)) {
            let alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
            let id: string;
            let fp: string;
            let ext: string = fileName.split(".").pop();

            do {
                id = "";
                for (let i = 0; i < 6; i++) {
                    let charNumber: number = Math.floor(Math.random() * alphabet.length);
                    id += alphabet.substring(charNumber, charNumber + 1);
                }
                fp = this.getBaseDir() + id + "." + ext;
            } while (FS.existsSync(fp));

            fileName = id + "." + ext;
        }

        let req: FileRequest = {
            originalFileName: orig,
            fileName: fileName,
            fileSize: fileSize,
            file: file,
        };

        this.req = req;

        return req;
    }

    protected getParam(name: string, defaultValue: any, doAddTrailingSlash: boolean): any {
        if (name in this.conf) return this.addTrailingSlash(this.conf[name], doAddTrailingSlash);
        else return defaultValue;
    }

    protected addTrailingSlash(value: string, doAddTrailingSlash: boolean): string {
        if (value != null && doAddTrailingSlash && (value.length == 0 || value.substring(value.length - 1) !== "/"))
            value += "/";
        return value;
    }

    public getBaseDir(): string {
        let d = __dirname;
        if (d.length === 1) d = d.substr(1); // "/" case

        let dir: string = this.getParam("dir", d + "files/", true);
        if (!FS.existsSync(dir)) {
            FS.mkdirSync(dir);
        }

        return dir;
    }

    public getTmpDir(): string {
        let dir: string = this.getParam("tmp", this.getBaseDir() + "tmp/", true);
        if (!FS.existsSync(dir)) FS.mkdirSync(dir);

        return dir;
    }

    public async doPost(request: Express.Request, response: Express.Response): Promise<boolean> {
        let req: FileRequest = this.getReq(request, response);

        let file = new AFile(req, this.getBaseDir());
        file.upload();

        return true;
    }
}

class AFile {
    constructor(private req: FileRequest, public baseDir: string) { }

    public getFile(): string { return this.getFullPath(); }
    public getFullPath(): string { return this.baseDir + this.req.fileName; }
    public upload() {
        try {
            this.req.fullPath = this.getFullPath();
            FS.writeFileSync(this.getFullPath(), this.req.file);
        } catch (e) {
            throw new Error(`Failed to write file: ${e}`);
        }
    }
}