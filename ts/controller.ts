import * as Express from "express";
import * as FS from "fs";
import * as Path from "path";
import * as Stream from "stream";

import { Bearer } from "./credentials";
import { getDocsAsHTML } from "./documentation";
import * as T from "./testing";
import * as U from "./utils";
import { isObject } from "./validator/util";

export const docCss = `
a{
	color:#775ad0;
	text-decoration:none;
}
a:hover{
	color:#b4b0c1;
}
hr{
	border-top:2px solid #f4f5f7;
	border-bottom:0px;
	border-left:0px;
	border-right:0px;
}
.mono{
	line-height:18px;
	font-size:12px;
	font-family:"Fira Code","Source Code Pro",Menlo,Monaco,monospace,sans-serif,-apple-system,BlinkMacSystemFont;
}
body{
	font-family:Arial;
	font-size:14px;
	padding: 0;
	margin: 0;
}
pre{
	line-height:18px;
	font-size:12px;
	font-family:"Fira Code","Source Code Pro",Menlo,Monaco,monospace,sans-serif,-apple-system,BlinkMacSystemFont;
}
.docbox{
	margin-bottom:10px;
	padding:40px;
	background:white;
	margin-right:auto;
	margin-left:auto;
	border-bottom: 1px solid #bbb;
}
.mountPointWrapper{
	background: #f4f5f7;
	padding: 5px;
	border: 1px solid #c8c5c5;
}
.path {
	font-size: 16px;
	color: blueviolet;
	margin-bottom: 15px;
	font-weight: 500;
}
`;

/**
 * docbox
.docbox {
	margin-bottom:10px;
	padding:10px;
	background:white;
	box-shadow:0px 0px 1px 1px #00000038, 0px 4px 8px rgba(0, 0, 0, 0.15);
	border-radius:2px;
	width:700px;
	margin-right:auto;
	margin-left:auto;
}
 */

export interface IDisposable {
	dispose(): void;
}

export class Context {
	request: Express.Request;
	response: Express.Response;
	nextMiddleware: Function;

	private disposableInstance: IDisposable[];

	constructor() {
		this.disposableInstance = [];
	}

	create<T extends IDisposable>(
		tFactory: { new (...args: any[]): T },
		...args: any[]
	): T {
		let instance = new tFactory(...args);
		this.disposableInstance.push(instance);

		return instance;
	}

	dispose(): void {
		for (let instance of this.disposableInstance) {
			instance.dispose();
		}
	}
}
// export interface PrismResponse<T> extends HttpError, HttpSuccess<T> {  }
// export type PrismResponse<T> = HttpError | HttpSuccess<T>;
// export interface PrismResponse<T> implements HttpSuccess<T>;

export class PrismResponse<T> {
	/**
	 * An HTTP status code like 200, 401, 404, etc.
	 *
	 * @type {number}
	 */
	public code: number;

	/**
	 * A message relevant to the result of this request.
	 *
	 * @type {string}
	 */
	public message?: string;

	/**
	 * Whether or not a server-side error occurred. Always defined,
	 * whether or not an HttpError or HttpSuccess<T> is returned, but
	 * only true when the response is an HttpError.
	 *
	 * @type {boolean}
	 */
	public error?: boolean;

	/**
	 * Whether or not the server considers your request a success. Always defined,
	 * whether or not an HttpError or HttpSuccess<T> is returned, but only
	 * true when the response is an HttpSuccess<T>.
	 *
	 * @type {boolean}
	 */
	public success?: boolean;

	/**
	 * A placeholder for an internal reference identifier for this request intended
	 * to be received from API consumers to help debug any particular request.
	 *
	 * @type {any}
	 */
	public reference?: any;

	/**
	 * A container for the T generic in HttpSuccess<T> when the response is an
	 * HttpSuccess<T>.
	 *
	 * @type {T}
	 */
	public data?: T;

	/**
	 * If a Bearer token was present in the request, it will be returned.
	 */
	public token?: string | undefined;

	constructor(a?: HttpError, b?: HttpSuccess<T>) {
		// if (a) return new HttpError(a.code, a.message);
		// if (b) return new HttpSuccess(b.code, b.reference, b.message, b.data);
		if (a) {
			this.code = a.code;
			this.error = true;
			this.success = undefined;
			this.message = a.message;
			this.token = a.token;
		}
		if (b) {
			this.code = b.code;
			this.error = undefined;
			this.success = true;
			this.message = b.message;

			this.data = b.data;
			this.reference = b.reference;
			this.token = b.token;
		}
	}
}

class HttpError {
	public success?: boolean | undefined = undefined;
	public error?: boolean | undefined = true;
	public code: number;
	public message?: string;
	public token?: string;

	constructor(context: Context, code: number, messageOrError: string | Error | Object) {
		this.code = code;
		this.token = Bearer.GetToken(context.request);

		if (messageOrError != undefined) {
			if (messageOrError instanceof Error) {
				this.message = messageOrError.message;
			} else if(isObject(messageOrError)) {
				this.message = JSON.stringify(messageOrError);
			} else {
				this.message = messageOrError.toString();
			}
		} else {
			this.message = "";
		}
	}
}

class HttpSuccess<T> implements HttpError {
	public code: number;
	public success?: boolean | undefined = true;
	public error?: boolean | undefined = undefined;
	public token: string;
	public data: any;
	public message: string;
	public reference: string | number;

	constructor(
		code: number,
		token: string,
		data: T,
		message: string,
		reference: number | string,
	) {
		this.code = code;
		this.token = token;
		this.data = data;
		this.message = message;
		this.reference = reference;
	}
}

class _Success<T> extends HttpSuccess<T> {
	constructor(
		context: Context,
		data: T = <T>{},
		message: string | undefined = undefined,
		reference: number | string | undefined = undefined
	) {
		if (context && context.response) context.response.status(HttpStatusCode.OK);
		let token = undefined;
		if (context && context.request) token = Bearer.GetToken(context.request);
		super(HttpStatusCode.OK, token, data, message, reference);
	}
}

export enum HttpStatusCode {
	OK = 200,
	Created = 201,
	BadRequest = 400,
	Unauthorized = 401,
	Forbidden = 403,
	NotFound = 404,
	UnprocessableEntity = 422,
	InternalServerError = 500,
	TooManyRequests = 429,
}

class _TooManyRequests extends HttpError {
	constructor(context: Context, messageOrError: string | Object | Error = "Too many requests") {
		if (context && context.response) context.response.status(HttpStatusCode.TooManyRequests);
		super(context, HttpStatusCode.TooManyRequests, messageOrError);
	}
}

class _BadRequest extends HttpError {
	constructor(context: Context, messageOrError: string | Object | Error = "Bad request") {
		if (context && context.response) context.response.status(HttpStatusCode.BadRequest);
		super(context, HttpStatusCode.BadRequest, messageOrError);
	}
}

class _Unauthorized extends HttpError {
	constructor(context: Context, messageOrError: string | Object | Error = "Unauthorized") {
		if (context && context.response) context.response.status(HttpStatusCode.Unauthorized);
		super(context, HttpStatusCode.Unauthorized, messageOrError);
	}
}

class _Forbidden extends HttpError {
	constructor(context: Context, messageOrError: string | Object | Error = "Forbidden") {
		if (context && context.response) context.response.status(HttpStatusCode.Forbidden);
		super(context, HttpStatusCode.Forbidden, messageOrError);
	}
}

class _NotFound extends HttpError {
	constructor(context: Context, messageOrError: string | Object | Error = "Not found") {
		if (context && context.response) context.response.status(HttpStatusCode.NotFound);
		super(context, HttpStatusCode.NotFound, messageOrError);
	}
}

class _InternalServerError extends HttpError {
	constructor(context: Context, messageOrError: string | Object | Error = "Internal server error") {
		if (context && context.response) context.response.status(HttpStatusCode.InternalServerError);
		super(context, HttpStatusCode.InternalServerError, messageOrError);
	}
}

export interface IResponse {
	TooManyRequests(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
	BadRequest(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
	Unauthorized(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
	NotFound(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
	Forbidden(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
	InternalServerError(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
	Success<T>(
		context: Context,
		data: T,
		message: string,
		reference: number | string
	): PrismResponse<T>;
}

class ResponsesClass implements IResponse {
	public TooManyRequests(context: Context, messageOrError?: string | Object | Error): PrismResponse<any> {
		return new PrismResponse<any>(new _TooManyRequests(context, messageOrError), null);
	}
	public BadRequest(context: Context, messageOrError?: string | Object | Error): PrismResponse<any> {
		return new PrismResponse<any>(new _BadRequest(context, messageOrError), null);
	}
	public Unauthorized(context: Context, messageOrError?: string | Object | Error): PrismResponse<any> {
		return new PrismResponse<any>(new _Unauthorized(context, messageOrError), null);
	}
	public NotFound(context: Context, messageOrError?: string | Object | Error): PrismResponse<any> {
		return new PrismResponse<any>(new _NotFound(context, messageOrError), null);
	}
	public Forbidden(context: Context, messageOrError?: string | Object | Error): PrismResponse<any> {
		return new PrismResponse<any>(new _Forbidden(context, messageOrError), null);
	}
	public InternalServerError(context: Context, messageOrError?: string | Object | Error): PrismResponse<any> {
		return new PrismResponse<any>(new _InternalServerError(context, messageOrError), null);
	}
	public Success<T>(
		context: Context,
		data?: T,
		message?: string,
		reference?: number | string
	): PrismResponse<T> {
		return new PrismResponse<T>(
			null,
			new _Success<T>(context, data, message, reference)
		);
	}
	public OK<T>(
		context: Context,
		data?: T,
		message?: string,
		reference?: number | string
	): PrismResponse<T> {
		return new PrismResponse<T>(
			null,
			new _Success<T>(context, data, message, reference)
		);
	}
}

export const Responses = new ResponsesClass();

export class FileServe {
	file: string | Stream.Readable;
	filename: string;
	contentType: string;
	forceAttachment: boolean;

	constructor(
		file: string | Stream.Readable,
		filename?: string,
		extension?: string,
		forceAttachment?: boolean
	) {
		this.file = file;
		this.filename =
			filename != undefined
				? filename
				: typeof file === "string"
				? Path.basename(file)
				: "file";
		extension = extension != undefined ? extension : this.filename;
		this.contentType = require("mime-types").contentType(extension);
		this.forceAttachment = !!forceAttachment;
	}

	sendHeaders(res: Express.Response) {
		res.setHeader("Accept-Ranges", "bytes");
		res.setHeader("Content-Type", this.contentType);
		res.setHeader(
			"Content-Disposition",
			`${this.forceAttachment ? "attachment" : "inline"}; filename=${
				this.filename
			}`
		);
	}
}

export let globalCtrState: PrismControllerState;

/**************************************************
 * Class Decorators
 **************************************************/

export function Controller<T>(
	mountpoint?: string | ControllerConstructor<T>,
	path?: string
): (f: Function) => void {
	return (ctr: Function) => {
		let c = globalCtrState.getOrInsertController(ctr);
		c.explicitlyDeclared = true;

		if (mountpoint !== undefined) {
			if (typeof mountpoint === "string") {
				c.path = mountpoint as string;
			} else {
				globalCtrState.registerMountpoint(mountpoint as any, ctr);
				c.path = typeof path === "string" ? path : ctr.name;
			}
		} else {
			c.path = ctr.name;
		}
		c.path = U.UrlJoin("/", c.path);
	};
}

/**
 * Adds express middleware to run before mounting the controller
 * @param { Express.RequestHandler[] } middleware - Array of middleware to add.
 */
export function ControllerMiddleware(
	...middleware: Express.RequestHandler[]
): (f: Function) => void {
	return (ctr: Function) => {
		if (middleware != undefined) {
			let c = globalCtrState.getOrInsertController(ctr);
			c.middleware = middleware.concat(c.middleware);
		}
	};
}

/**
 * @param { boolean } condition - Only mounts this controller if condition is true.
 */
export function MountCondition(condition: boolean): (f: Function) => void {
	return (ctr: Function) => {
		let c = globalCtrState.getOrInsertController(ctr);
		c.mountCondition = c.mountCondition && condition;
	};
}

/**
 *  Only mounts this controller if NODE_ENV is set to "development"
 */
export function Dev(): (f: Function) => void {
	return MountCondition(process.env.NODE_ENV === "development");
}

/**
 *  Attach a documentation string to the controller
 *  @param {string} docStr - The documentation string.
 */
export function DocController(docStr: string): (f: Function) => void {
	return (ctr: Function) => {
		globalCtrState.getOrInsertController(ctr).docString = docStr;
	};
}

/**
 * Generate test runner paths inside this controller
 */
export function TestRunner(): (f: Function) => void {
    return (ctr: Function) => {
        globalCtrState.getOrInsertController(ctr).generateTestRunnerPaths = true;
    };
}

/**************************************************
 * Method Decorators
 **************************************************/

export function Method(
	method: string,
	path?: string
): (a: any, s: string, pd: PropertyDescriptor) => void {
	return function (
		target: any,
		propertyKey: string,
		descriptor: PropertyDescriptor
	) {
		path = path != undefined ? path : propertyKey;
		method = method.toLowerCase();
		let m = globalCtrState
			.getOrInsertController(target.constructor)
			.getOrInsertMethod(propertyKey);
		m.methodMountpoints.push({
			path: U.UrlJoin("/", path),
			httpMethod: method,
		});
		m.explicitlyDeclared = true;
	};
}

export function GET(
	path?: string
): (a: any, s: string, pd: PropertyDescriptor) => void {
	return Method("get", path);
}

export function POST(
	path?: string
): (a: any, s: string, pd: PropertyDescriptor) => void {
	return Method("post", path);
}

export function PUT(
	path?: string
): (a: any, s: string, pd: PropertyDescriptor) => void {
	return Method("put", path);
}

export function PATCH(
	path?: string
): (a: any, s: string, pd: PropertyDescriptor) => void {
	return Method("patch", path);
}

export function DELETE(
	path?: string
): (a: any, s: string, pd: PropertyDescriptor) => void {
	return Method("delete", path);
}

// export function Upload(
// 	path?: string
// ): (a: any, s: string, pd: PropertyDescriptor) => void {
// 	return Method("post", path);
// }

/**
 * Adds express middleware to run before the method
 * @param { Express.RequestHandler[] } middleware - Array of middleware to add.
 */
export function ActionMiddleware(
	...middleware: Express.RequestHandler[]
): (a: any, s: string, pd: PropertyDescriptor) => void {
	return function (
		target: any,
		propertyKey: string,
		descriptor: PropertyDescriptor
	) {
		if (middleware != undefined) {
			let m = globalCtrState
				.getOrInsertController(target.constructor)
				.getOrInsertMethod(propertyKey);
			m.middleware = middleware.concat(m.middleware);
		}
	};
}

/**
 * Flags the method as "Express Compatible" and thus will be called with parameters (req,res,next)
 */
export function ExpressCompatible(): (
	a: any,
	s: string,
	pd: PropertyDescriptor
) => void {
	return function (
		target: any,
		propertyKey: string,
		descriptor: PropertyDescriptor
	) {
		let m = globalCtrState
			.getOrInsertController(target.constructor)
			.getOrInsertMethod(propertyKey);
		m.expressCompatible = true;
	};
}

/**
 *  Attach a documentation string to the method
 *  @param {string} docStr - The documentation string.
 */
export function DocAction(
	docStr: string
): (a: any, s: string, pd: PropertyDescriptor) => void {
	return function (
		target: any,
		propertyKey: string,
		descriptor: PropertyDescriptor
	) {
		let m = globalCtrState
			.getOrInsertController(target.constructor)
			.getOrInsertMethod(propertyKey);
		m.docString = docStr;
	};
}

/**************************************************
 * Method Parameter Decorators
 **************************************************/

export type RequestValueContainer =
	| "body"
	| "query"
	| "path"
	| "header"
	| "cookie";

type SimpleFunction = (val: any) => any;

export function MapParameterToRequestValue(
	rvc: RequestValueContainer,
	valueKey: string,
	openApiType?: string,
	modFunction?: SimpleFunction,
): (
	target: Object,
	propertyKey: string | symbol,
	parameterIndex: number
) => void {
	return function (target: any, propertyKey: string, parameterIndex: number) {
		let m = globalCtrState
			.getOrInsertController(target.constructor)
			.getOrInsertMethod(propertyKey);
		m.extraParametersMappings[parameterIndex] = {
			rvc: rvc,
			valueKey: valueKey,
			openApiType: openApiType,
			modFunction: modFunction,
		};
	};
}

export function FromBody(
	valueKey?: string,
	modFunction?: SimpleFunction,
	openApiType?: string
): (
	target: Object,
	propertyKey: string | symbol,
	parameterIndex: number
) => void {
	return MapParameterToRequestValue(
		"body",
		valueKey,
		openApiType,
		modFunction
	);
}

export function FromQuery(
	valueKey: string,
	modFunction?: SimpleFunction,
	openApiType?: string
): (
	target: Object,
	propertyKey: string | symbol,
	parameterIndex: number
) => void {
	return MapParameterToRequestValue(
		"query",
		valueKey,
		openApiType,
		modFunction
	);
}

export function FromPath(
	valueKey: string,
	modFunction?: SimpleFunction,
	openApiType?: string
): (
	target: Object,
	propertyKey: string | symbol,
	parameterIndex: number
) => void {
	return MapParameterToRequestValue(
		"path",
		valueKey,
		openApiType,
		modFunction
	);
}

export function FromHeader(
	valueKey: string,
	modFunction?: SimpleFunction,
	openApiType?: string
): (
	target: Object,
	propertyKey: string | symbol,
	parameterIndex: number
) => void {
	return MapParameterToRequestValue(
		"header",
		valueKey,
		openApiType,
		modFunction
	);
}
export function FromCookie(
	valueKey: string,
	modFunction?: SimpleFunction,
	openApiType?: string
): (
	target: Object,
	propertyKey: string | symbol,
	parameterIndex: number
) => void {
	return MapParameterToRequestValue(
		"cookie",
		valueKey,
		openApiType,
		modFunction
	);
}

/*********************************************************
 * Utils
 *********************************************************/

export function DumpInternals(): void {
	for (let ck in globalCtrState.controllers) {
		console.log("============================================");
		console.log(
			`Controller on path ${globalCtrState.controllers[ck].path} built from class ${globalCtrState.controllers[ck].ctr.name}`
		);
		console.log("Methods:");
		for (let mk in globalCtrState.controllers[ck].methods) {
			let m = globalCtrState.controllers[ck].methods[mk];
			console.log(`== ${mk} ==`);
			console.log(m);
			console.log("");
		}
	}
}

/**************************************************
 * Internals
 **************************************************/

export type PrismMethodMountpoint = { path: string; httpMethod: string };
export type PrismExtraParametersMapping = {
	rvc: RequestValueContainer;
	valueKey: string;
	openApiType: string;
	modFunction: Function;
};

export class PrismMethodOpenApiResponses {
	[httpCode: string]: { description: string; type: string };
}

export class PrismMethod {
	docString = "";
	expressCompatible = false;
	explicitlyDeclared = false;
	middleware: Express.RequestHandler[] = [];
	methodMountpoints: PrismMethodMountpoint[] = [];
	openApiResponses: PrismMethodOpenApiResponses = {};
	extraParametersMappings: PrismExtraParametersMapping[] = [];
}

export type PrismMethodMap = { [key: string]: PrismMethod };
export type ControllerConstructor<T> = { new (...args: any[]): T };

export class PrismController {
	path: string;
	ctr: Function;
	docString = "";
	mountCondition = true;
	router: Express.Router;
	generateTestRunnerPaths: boolean = false;
	childController = false;
	explicitlyDeclared = false;
	methods: PrismMethodMap = {};
	middleware: Express.RequestHandler[] = [];
	node: PrismControllerTreeNode = undefined;

	getOrInsertMethod(key: string): PrismMethod {
		if (this.methods[key] === undefined) {
			this.methods[key] = new PrismMethod();
		}

		return this.methods[key];
	}
}

export class PrismControllerTreeNode {
	fullPath: string;
	controller: PrismController;
	children: PrismControllerTreeNode[] = [];

	constructor(controller: PrismController) {
		this.controller = controller;
		this.fullPath = controller.path;
	}
}

export type PrismControllerMap = { [key: string]: PrismController };
export type PrismMountpoint = { dstCtr: Function; ctr: Function };

export class PrismControllerState {
	mountpoints: PrismMountpoint[] = [];
	controllers: PrismControllerMap = {};
	controllersTree: PrismControllerTreeNode[] = [];

	getController(ctr: Function): PrismController {
		return this.controllers[ctr.toString()];
	}

	getOrInsertController(ctr: Function): PrismController {
		let key = ctr.toString();

		if (this.controllers[key] === undefined) {
			this.controllers[key] = new PrismController();
			this.controllers[key].ctr = ctr;
		}

		return this.controllers[key];
	}

	registerMountpoint(dstCtr: any, ctr: Function): void {
		this.getOrInsertController(ctr).childController = true;
		this.mountpoints.push({ dstCtr: dstCtr, ctr: ctr });
	}
}

globalCtrState = new PrismControllerState();

function addChildrenToTreeNode(node: PrismControllerTreeNode): void {
	node.controller.node = node;

	for (let mp of globalCtrState.mountpoints) {
		if (node.controller.ctr.toString() === mp.dstCtr.toString()) {
			let child = new PrismControllerTreeNode(
				globalCtrState.getController(mp.ctr)
			);
			addChildrenToTreeNode(child);
			node.children.push(child);
		}
	}
}

function buildControllersTree() {
	for (let ck in globalCtrState.controllers) {
		let c = globalCtrState.controllers[ck];

		if (c.childController === false) {
			let node = new PrismControllerTreeNode(c);
			addChildrenToTreeNode(node);
			globalCtrState.controllersTree.push(node);
		}
	}
}

function indexAutogenerator(
	controller: PrismController,
	children: PrismControllerTreeNode[]
): (req: Express.Request, res: Express.Response) => void {
	return (req: Express.Request, res: Express.Response) => {
		let content = `<html><head><style>${docCss}</style></head><body>`;

		if (children && children.length) {
			content += `<div class="docbox">`;
			let i = 0;
			for (let child of children) {
				content += `<div><a class="mono" href=".${child.controller.path}/">${child.controller.path}</a></div>`;
				content += `<div style="color:#777;font-weight:400;">${child.controller.docString}</div>`

				if (i < children.length - 1) {
					content += `<hr />`;
				}
				
				i++;

			}
			content += `</div>`;
		}

		if (controller !== undefined) {
			content += `<div class="docbox">`
			content += `<div>Viewing routes for controller <strong>${controller.ctr.name}</strong>:</div><br />`;
			let ml = Object.keys(controller.methods);
			let i = 0;
			for (let mk in controller.methods) {
				content += `<div style="line-height:18px;">`;
				for (let mmp of controller.methods[mk].methodMountpoints) {
					content += `<span class="mono" style="color:#777;width:40px;display:inline-block;">${mmp.httpMethod.toUpperCase()}</span>`
					content += `<a href=".${mmp.path}/" class="mono">${controller.path}${mmp.path}</a>`;
					content += `<div style="color:#777;margin-left:40px;">${controller.methods[mk].docString}</div>`;

					if (i < ml.length - 1) {
						content += `<hr />`;
					}
					i++;
				}
				content += `</div>`;
			}

			content += `</div>`;
		}

		content += "</body></html>";
		res.send(content);
	};
}

function mountMethod(
	controller: PrismController,
	instance: any,
	methodKey: string
): void {
	let method: PrismMethod = controller.methods[methodKey];

	if (method.explicitlyDeclared === false) {
		U.defaultWarnLogger(
			`Method "${methodKey}" was not explicitly declared with an HTTP request type decorator (e.g. @Prism.GET()). Defaulting to GET /${methodKey}`
		);
		method.methodMountpoints.push({
			path: `/${methodKey}`,
			httpMethod: "get",
		});
	}

	for (let mp of method.methodMountpoints) {
		let callback = (
			req: Express.Request,
			res: Express.Response,
			next: Function
		) => {
			let context = new Context();

			let runner = async () => {
				let ret: any;

				if (method.expressCompatible) {
					ret = instance[methodKey](req, res, next);
				} else {
					context.request = req;
					context.response = res;
					context.nextMiddleware = next;

					let params: any[] = [context];

					if (method.extraParametersMappings[0] !== undefined) {
						throw new Error(
							"Cannot map first parameter, it should always be Context"
						);
					}

					for (
						let i = 1;
						i < method.extraParametersMappings.length;
						i++
					) {
						let mp = method.extraParametersMappings[i];

						if (mp === undefined) {
							params.push(undefined);
						} else {
							switch (mp.rvc) {
								case "body":
									if (
										mp.valueKey === undefined ||
										mp.valueKey === ""
									) {
										params.push(req.body);
									} else {
										params.push(req.body[mp.valueKey]);
									}
									break;
								case "query":
									params.push(req.query[mp.valueKey]);
									break;
								case "path":
									params.push(req.params[mp.valueKey]);
									break;
								case "header":
									params.push(req.headers[mp.valueKey]);
									break;
								case "cookie":
									params.push(req.cookies[mp.valueKey]);
									break;
							}

							// TODO: Maybe instead of throwing, send a BadRequest response.
							if (mp.modFunction) {
								try {
									let x = mp.modFunction(
										params[params.length - 1]
									);
									params[params.length - 1] = x;
								} catch (e) {
									throw new Error(
										`Failed to execution modFunction (${
											mp.modFunction
										}) on parameter (${mp.valueKey}: ${
											params[params.length - 1]
										})`
									);
								}
							}
						}
					}

					ret = instance[methodKey](...params);
				}

				if (ret instanceof Promise) {
					ret = await ret;
				}

				if (ret instanceof FileServe) {
					// serveFile(res, ret);
				} else if (ret instanceof Object) {
					if (ret.$render_view !== undefined) {
						res.render(ret.$render_view, ret);
					} else {
						res.json(ret);
					}
				} else if (typeof ret === "string") {
					res.send(ret);
				}
			};

			runner()
				.then(() => {
					context.dispose();
				})
				.catch((err) => {
					context.dispose();
					next(err);
				});
		};

		// TODO: Add throttle stuff to middleware
		controller.router[mp.httpMethod](
			U.UrlJoin(mp.path, "/"),
			...method.middleware,
			callback
		);
	}
}

function useRouterAtPathStrict(
	baseRouter: Express.Router,
	basePath: string,
	router: Express.Router
): void {
	if (basePath.substring(basePath.length - 1) === "/") {
		basePath = basePath.trim().substr(0, basePath.length - 1);
	}

	let strictPath = U.UrlJoin(basePath, "/");

	if (strictPath !== "/") {
		baseRouter.use(strictPath, (req: any, res: any, next: any) => {
				if (req.originalUrl.substring(req.originalUrl.length - basePath.length) === basePath) {
					res.redirect(strictPath);
				} else {
					next();
				}
			},
			router
		);
	} else {
		baseRouter.use(strictPath, router);
	}
}

function createRouterRecursive(
	app: Express.Application,
	controllerNode: PrismControllerTreeNode
): PrismController {
	let controller = controllerNode.controller;
	controllerNode.fullPath = controller.path;

	if (controller.mountCondition === false) {
		return undefined;
	}

	if (controller.explicitlyDeclared === false) {
		U.defaultWarnLogger(
			`Controller "${controller.ctr.name}" was not explicitly declared with a @Controller decorator.`
		);
	}

	let instance = Reflect.construct(controller.ctr, []);
	controller.router = Express.Router({ mergeParams: true });

	for (let middleware of controller.middleware) {
		controller.router.use(middleware);
	}

	for (let mk in controller.methods) {
		mountMethod(controller, instance, mk);
	}

	for (let child of controllerNode.children) {
		let nc = createRouterRecursive(app, child);

		if (nc !== undefined) {
			useRouterAtPathStrict(controller.router, nc.path, nc.router);
			child.fullPath = U.UrlJoin(
				controllerNode.fullPath,
				"/",
				child.fullPath
			);
		}
	}

	if (controller.generateTestRunnerPaths) {
		T.injectTestRunnerMiddleware(controller);
	}

	if (process.env.NODE_ENV === "development") {
		controller.router.get(
			"/",
			indexAutogenerator(controller, controllerNode.children)
		);
	}

	return controller;
}

function handleRequestErrorMiddleware(
	err: any,
	req: Express.Request,
	res: Express.Response,
	next: Function
): void {
	for (let i = 0; i < U.errorHandlers.length - 1; i++) {
		U.errorHandlers[i](err, req, res, U.errorHandlers[i + 1]);
	}

	if (U.errorHandlers.length > 0) {
		U.errorHandlers[U.errorHandlers.length - 1](
			err,
			req,
			res,
			onRequestError
		);
	} else {
		onRequestError(err, req, res, next);
	}
}

function onRequestError(
	err: any,
	req: Express.Request,
	res: Express.Response,
	next: Function
): void {
	let c = new Context();
	c.request = req;
	c.response = res;
	c.nextMiddleware = next;
	if (err.name === "UnauthorizedError") {
		res.sendStatus(401);
	} else {
		if (process.env.NODE_ENV === "development") {
			res.statusCode = 500;
			if (err instanceof HttpError) {
				U.defaultErrorLogger(err);
				// res.status(err.code).send(err.message);
                res.json(new PrismResponse<any>(
                    new _InternalServerError(c, err.message),
                    null
                ));
			} else if (err instanceof Error) {
				U.defaultErrorLogger({
					name: err.name,
					message: err.message,
					stack: err.stack,
				});
                // An unexpected error probably happened.
                // Send an InternalServerError HttpError as a PrismResponse.
                res.json(new PrismResponse<any>(
                    new _InternalServerError(c, err.message),
                    null
                ));
				// res.json({
				// 	code: HttpStatusCode.InternalServerError,
				// 	error: true,
				// 	success: false,
				// 	name: err.name,
				// 	message: err.message,
				// });
			} else {
				U.defaultErrorLogger(err);
				// res.json(err);
                res.json(new PrismResponse<any>(
                    new _InternalServerError(c, err),
                    null
                ));
			}
		} else {
			res.sendStatus(500);
		}
	}
}

function onRequestNotFound(
	req: Express.Request,
	res: Express.Response,
	next: Function
): void {
	let c = new Context();
	c.request = req;
	c.response = res;
	c.nextMiddleware = next;
	res.status(404).send(
		new PrismResponse<any>(new _NotFound(c, "Route not defined."), null)
	);
	// res.sendStatus(500);
}

export let initialized = false;

export function initialize(
	app: Express.Application,
	...requiredDirectories: string[]
): void {
	initializeAtRoute("/", app, ...requiredDirectories);
}

export function initializeAtRoute(
	rootPath: string,
	app: Express.Application,
	...requiredDirectories: string[]
): void {
	let implicitTests = false;
	let implicitControllers = false;

	if (
		!requiredDirectories.find((p) => {
			return p === "tests";
		})
	) {
		requiredDirectories.push("tests");
		implicitTests = true;
	}

	if (
		!requiredDirectories.find((p) => {
			return p === "controllers";
		})
	) {
		requiredDirectories.push("controllers");
		implicitControllers = true;
	}

	for (let requiredDirectory of requiredDirectories) {
		let path = "";

		if (Path.isAbsolute(requiredDirectory)) {
			path = requiredDirectory;
		} else {
			path = Path.join(process.cwd(), requiredDirectory);
		}

		try {
			// U.defaultInfoLogger("Loading components from: " + path);
			FS.accessSync(path);
		} catch (err) {
			if (
				(requiredDirectory !== "controllers" || !implicitControllers) &&
				(requiredDirectory !== "tests" || !implicitTests)
			) {
				U.defaultWarnLogger("Cannot access path: " + path);
			}

			continue;
		}

		require("require-all")(path);
	}

	rootPath = rootPath || "/";
	buildControllersTree();

	for (let node of globalCtrState.controllersTree) {
		let nc = createRouterRecursive(app, node);

		if (nc !== undefined) {
			useRouterAtPathStrict(app, U.UrlJoin(rootPath, nc.path), nc.router);
			node.fullPath = U.UrlJoin(rootPath, "/", node.fullPath);
		}
	}

	if (process.env.NODE_ENV === "development") {
		app.get(
			rootPath,
			indexAutogenerator(undefined, globalCtrState.controllersTree)
		);
	}

	app.use(handleRequestErrorMiddleware);
	app.use(onRequestNotFound);
	initialized = true;
}

@Dev()
@Controller("/__dev")
@DocController("This development API reference controller is only mounted when process.NODE_ENV is 'development'.")
class PrismInternalDevelopmentController {
	@GET("/")
	index(): string {
		return getDocsAsHTML();
	}
}


@Dev()
@TestRunner()
@Controller("/__test")
@DocController("This interactive test runner is only mounted when process.NODE_ENV is 'development'.")
class PrismInternalTestController {}