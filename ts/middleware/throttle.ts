import * as Express from "express";
import { Context, Respond, Response } from "..";


let connections = {};

/**
 * 
 * @param maxPerSecond: Maximum number of requests allowed by an IP per second.
 * @returns 
 */
export function Throttle(maxPerSecond): Express.Handler {
    return function ThrottleMiddleware(req: Express.Request, res: Express.Response, next: Express.NextFunction): void | Express.Response<Response<any>> {
        // If we have an instance of req.ip in connections, increment it by 1.
        // Otherwise, set to 1.
        connections[req.ip] = connections[req.ip] ? connections[req.ip] + 1 : 1;

        // Every second, decrement instances of req.ip in connections.
        setTimeout(() => {
            connections[req.ip] = connections[req.ip] - 1;
            // If the new count is 0, remove our reference to req.ip.
            if (connections[req.ip] === 0) {
                delete connections[req.ip];
            }
        }, 1000);

        // If the number of instances exceeds the maximum allowed requests per second.
        if (connections[req.ip] > maxPerSecond) {
            let c = new Context();
            c.request = req;
            c.response = res;
            c.nextMiddleware = next;
            // Send a TooManyRequests response.
            return res.json(Respond.TooManyRequests(c));
        } else {
            return next();
        }
    }
}