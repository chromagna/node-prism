import { AtomicHandler } from "./atomic";
import { isEmail } from "../email";

export class StringHandler extends AtomicHandler<string> {
  constructor() {
    super();
    this._rules.push((v) => typeof v === "string" || "Must be a string");
  }

  prefix(substr: string): this {
    return this.startsWith(substr);
  }

  suffix(substr: string): this {
    return this.endsWith(substr);
  }

  endsWith(substr: string): this {
    this._rules.push(
      (v) => v.endsWith(substr) || `Value has to end with ${substr}`,
    );
    return this;
  }

  beginsWith(substr: string): this {
    return this.startsWith(substr);
  }

  startsWith(substr: string): this {
    this._rules.push(
      (v) => v.startsWith(substr) || `Value has to start with ${substr}`,
    );
    return this;
  }

  email(): this {
    this._rules.push((v) => isEmail(v) || `Must be a valid email`);
    return this;
  }

  numeric(): this {
    const regexp = /[^0-9]/;
    this._rules.push((v) => (v.length && !regexp.test(v)) || `Must be numeric`);
    return this;
  }

  alphanum(allowSpaces?: boolean): this {
    const regexp = allowSpaces ? /[^a-zA-Z0-9 ]/ : /[^a-zA-Z0-9]/;
    this._rules.push(
      (v) => (v.length && !regexp.test(v)) || `Must be alphanumeric`,
    );
    return this;
  }

  regex(regexp: RegExp): this {
    this._rules.push(
      (v: string) => regexp.test(v) || `Does not match ${regexp.toString()}`,
    );
    return this;
  }

  match(regexp: RegExp): this {
    return this.regex(regexp);
  }

  pattern(regexp: RegExp): this {
    return this.regex(regexp);
  }

  length(num: number): this {
    this._rules.push(
      (v: string) => v.length === num || `Must be of length ${num}`,
    );
    return this;
  }

  len(num: number): this {
    return this.length(num);
  }

  notEmpty(/* TODO: trim? */): this {
    return this.min(1);
  }

  between(min: number, max: number): this {
    return this.min(min).max(max);
  }

  min(min: number): this {
    this._rules.push(
      (v: string) =>
        v.length >= min || `Must be at least ${min} characters long`,
    );
    return this;
  }

  max(max: number): this {
    this._rules.push(
      (v: string) => v.length <= max || `Must have at most ${max} characters`,
    );
    return this;
  }
}
