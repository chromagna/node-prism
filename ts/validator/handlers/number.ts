import { AtomicHandler } from "./atomic";

export class NumberHandler extends AtomicHandler<number> {
  constructor() {
    super();
    this._rules.push(
      (v: unknown) =>
        (typeof v === "number" && !isNaN(v)) || "Must be a number",
    );
  }

  natural(opts?: Partial<{ withZero: boolean }>): this {
    return this.integer().positive({ withZero: opts?.withZero });
  }

  int(): this {
    return this.integer();
  }

  integer(): this {
    this._rules.push(
      (v: number) => Number.isInteger(v) || `Must be an integer`,
    );
    return this;
  }

  negative(opts?: Partial<{ withZero: boolean }>): this {
    if (opts?.withZero) {
      this.max(0);
    } else {
      this._rules.push((v: number) => v < 0 || `Must be a negative number`);
    }
    return this;
  }

  positive(opts?: Partial<{ withZero: boolean }>): this {
    if (opts?.withZero) {
      this.min(0);
    } else {
      this._rules.push((v: number) => v > 0 || `Must be a positive number`);
    }
    return this;
  }

  between(min: number, max: number): this {
    return this.min(min).max(max);
  }

  min(min: number): this {
    this._rules.push((v: number) => v >= min || `Must be ${min} or greater`);
    return this;
  }

  max(max: number): this {
    this._rules.push(
      (v: number) => v <= max || `Must be less than or equal ${max}`,
    );
    return this;
  }
}
