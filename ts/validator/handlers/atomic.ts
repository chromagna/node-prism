import { BaseHandler } from "./base";

/**
 * Base handler for atomic values
 */
export abstract class AtomicHandler<
  T = string | number | boolean
> extends BaseHandler<T> {
  _type!: T;

  equals(expected: T): this {
    this._rules.push(
      (v: T) => v === expected || `Must be equal to ${expected}`,
    );
    return this;
  }

  eq(expected: T): this {
    return this.equals(expected);
  }

  equal(expected: T): this {
    return this.equals(expected);
  }

  enum(values: T[]): this {
    this._rules.push(
      (v: T) =>
        values.includes(v) ||
        `Must be one of the following values: ${values.join(", ")}`,
    );
    return this;
  }
}
