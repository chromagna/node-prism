import { IValidationResult, Rule } from "../types";
import { log } from "../log";

export abstract class Handler<T = any> {
  _type!: any;

  protected _rules: Rule<T>[] = [];

  test(rule: Rule<T>): this {
    this.custom(rule);
    return this;
  }

  check(rule: Rule<T>): this {
    this.custom(rule);
    return this;
  }

  use(rule: Rule<T>): this {
    this.custom(rule);
    return this;
  }

  rule(rule: Rule<T>): this {
    this.custom(rule);
    return this;
  }

  custom(rule: Rule<T>): this {
    this._rules.push(rule);
    return this;
  }

  validate(
    value: unknown,
    key?: string[],
    root?: unknown,
  ): IValidationResult[] {
    const results: { key: string[]; message: string | boolean }[] = [];

    log("Checking rules");
    for (const rule of this._rules) {
      const result = rule(<any>value, key || [], root || value);
      if (typeof result === "string" || !result) {
        log("Rule failed!");
        results.push({ key: key || [], message: result });
        log("Rule result: " + result);
        return results;
      } else {
        log("Rule passed!");
      }
    }

    log(`Checked rules, ${results.length} errors`);
    return results;
  }
}
