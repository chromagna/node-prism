export function log(...args: unknown[]): void {
  if (process.env.DEVELOPMENT) {
    console.error(...args);
  }
}
