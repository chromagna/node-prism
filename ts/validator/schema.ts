import { Handler } from "./handlers";
import { ObjectHandler } from "./handlers/object";
import { Infer, ISchemaDefinition } from "./types";

function getResultMessage(result: Array<any>): string {
  let s = "";

  for (const err in result) {
    for (const k of Object.keys(err)) {
      s += `"${(k)}": ${(k as any).message}. `
    }
  }

  return s.trim();
}

export function createExecutableSchema(handler: Handler) {
  return (value: unknown) => {
    const result = handler.validate(value, [], value);
    return {
      ok: !result.length,
      errors: result,
      message: getResultMessage(result),
    };
  };
}

export function createSchema(def: ISchemaDefinition) {
  return createExecutableSchema(new ObjectHandler(def));
}

export function is<T extends Handler>(
  value: unknown,
  handler: T,
): value is Infer<T> {
  return createExecutableSchema(handler)(value).ok;
}
