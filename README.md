# node-prism

Prism is a backend development framework.

[toc]

# Quickstart

The example below showcases a few of the different available decorators. They are mostly self-explanatory.

```javascript
import {
    Controller,
    DocController,
    ControllerMiddleware,
    ActionMiddleware,
    Middleware,
    Context,
    Response,
    POST,
    GET,
    FromPath,
} from "node-prism";

@Controller("/photo")
@DocController("This controller provides a REST interface to the Photo entity.")
@ControllerMiddleware(PhotoController.LoggingMiddleware)
class PhotoController {
    static LoggingMiddleware(req: Express.Request, res: Express.Response, next: Function) {
        next();
    }

    @POST("/upload")
    @DocAction("This endpoint handles photo uploads.")
    @ActionMiddleware(Middleware.Throttle(2)) // 2 requests per second per host
    async upload(context: Context): Promise<Response<Photo>> {
        return PhotoService.UploadPhoto(context,
        {
            dir: "uploads",
            scrambleFilename: true,
            formFieldName: "file",
            returnBuffer: false,
        })
        .then((photo: Photo) => {
            return Respond.OK(context, photo, "Photo uploaded");
        })
        .catch((e: Error) => {
            return Respond.InternalServerError(context, e);
        });
    }

    @GET("")
    @DocAction("Get all photos.")
    async getAllPhotos(context: Context): Promise<Photo[]> {
        return PhotoService.GetAllPhotos()
            .then((photos: Photo[]) => {
                return Respond.OK(context, photos);
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }

    @GET("/:id")
    @DocAction("Get a photo by ID.")
    async getPhoto(context: Context, @FromPath("id", parseInt) id: number): Promise<Response<Photo>> {
        return PhotoService.GetPhotoById(id)
            .then((photo: Photo) => {
                return Respond.OK(context, photo);
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }
}
```

# Self-documenting API

If `process.env.NODE_ENV` is `development`, prism will mount a `__dev` endpoint, available at `host:port/__dev`. Here's what the docs would look like for the above controller:

![sample API docs](./.readme/sample_docs.PNG)

# Example project

You can check out a contrived example project here:

https://gitlab.com/chromagna/node-prism-example

The example includes:

- TORM for DB abstraction.
- JWT authentication and token validation middleware.
- Request throttling middleware.
- TCP command server.
- Sending email with nodemailer.