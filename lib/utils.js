"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WrappedPost = exports.WrappedGet = exports.UrlJoin = exports.defaultInfoLogger = exports.defaultWarnLogger = exports.defaultErrorLogger = exports.errorHandlers = void 0;
const axios_1 = require("axios");
exports.errorHandlers = [];
let defaultErrorLogger = (toLog) => {
    console.error(toLog);
};
exports.defaultErrorLogger = defaultErrorLogger;
let defaultWarnLogger = (toLog) => {
    console.warn(toLog);
};
exports.defaultWarnLogger = defaultWarnLogger;
let defaultInfoLogger = (toLog) => {
    console.log(toLog);
};
exports.defaultInfoLogger = defaultInfoLogger;
function UrlJoin(...parts) {
    let ret = parts.join("/");
    // remove consecutive slashes
    ret = ret.replace(/([^\/]*)\/+/g, "$1/");
    // make sure protocol is followed by two slashes
    ret = ret.replace(/(:\/|:\/\/)/g, "://");
    // remove trailing slash before parameters or hash
    ret = ret.replace(/\/(\?|&|#[^!])/g, "$1");
    // replace ? in parameters with &
    ret = ret.replace(/(\?.+)\?/g, "$1&");
    return ret;
}
exports.UrlJoin = UrlJoin;
function WrapRequest(p) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            return [undefined, yield p];
        }
        catch (err) {
            return [err, undefined];
        }
    });
}
/**
 *
 * @param url
 * @param config AxiosRequestConfig
 * @returns Promise<Response<T>>
 */
function Get(url, config = undefined) {
    return new Promise((res, rej) => {
        axios_1.default
            .get(url, config)
            .then((r) => {
            if (r.data.error)
                rej(r.data);
            res(r.data);
        })
            .catch(err => rej(err));
    });
}
function Post(url, postdata = undefined, config = undefined) {
    return new Promise((res, rej) => {
        axios_1.default
            .post(url, postdata, config)
            .then((r) => {
            if (r.data.error)
                rej(r.data);
            res(r.data);
        })
            .catch(err => rej(err));
    });
}
function WrappedGet(url, config = undefined) {
    return __awaiter(this, void 0, void 0, function* () {
        return WrapRequest(Get(url, config));
    });
}
exports.WrappedGet = WrappedGet;
function WrappedPost(url, postdata = undefined, config = undefined) {
    return __awaiter(this, void 0, void 0, function* () {
        return WrapRequest(Post(url, postdata, config));
    });
}
exports.WrappedPost = WrappedPost;
//# sourceMappingURL=utils.js.map