"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initializeAtRoute = exports.initialize = exports.initialized = exports.PrismControllerState = exports.PrismControllerTreeNode = exports.PrismController = exports.PrismMethod = exports.PrismMethodOpenApiResponses = exports.DumpInternals = exports.FromCookie = exports.FromHeader = exports.FromPath = exports.FromQuery = exports.FromBody = exports.MapParameterToRequestValue = exports.DocAction = exports.ExpressCompatible = exports.ActionMiddleware = exports.DELETE = exports.PATCH = exports.PUT = exports.POST = exports.GET = exports.Method = exports.TestRunner = exports.DocController = exports.Dev = exports.MountCondition = exports.ControllerMiddleware = exports.Controller = exports.globalCtrState = exports.FileServe = exports.Responses = exports.HttpStatusCode = exports.PrismResponse = exports.Context = exports.docCss = void 0;
const Express = require("express");
const FS = require("fs");
const Path = require("path");
const credentials_1 = require("./credentials");
const documentation_1 = require("./documentation");
const T = require("./testing");
const U = require("./utils");
const util_1 = require("./validator/util");
exports.docCss = `
a{
	color:#775ad0;
	text-decoration:none;
}
a:hover{
	color:#b4b0c1;
}
hr{
	border-top:2px solid #f4f5f7;
	border-bottom:0px;
	border-left:0px;
	border-right:0px;
}
.mono{
	line-height:18px;
	font-size:12px;
	font-family:"Fira Code","Source Code Pro",Menlo,Monaco,monospace,sans-serif,-apple-system,BlinkMacSystemFont;
}
body{
	font-family:Arial;
	font-size:14px;
	padding: 0;
	margin: 0;
}
pre{
	line-height:18px;
	font-size:12px;
	font-family:"Fira Code","Source Code Pro",Menlo,Monaco,monospace,sans-serif,-apple-system,BlinkMacSystemFont;
}
.docbox{
	margin-bottom:10px;
	padding:40px;
	background:white;
	margin-right:auto;
	margin-left:auto;
	border-bottom: 1px solid #bbb;
}
.mountPointWrapper{
	background: #f4f5f7;
	padding: 5px;
	border: 1px solid #c8c5c5;
}
.path {
	font-size: 16px;
	color: blueviolet;
	margin-bottom: 15px;
	font-weight: 500;
}
`;
class Context {
    constructor() {
        this.disposableInstance = [];
    }
    create(tFactory, ...args) {
        let instance = new tFactory(...args);
        this.disposableInstance.push(instance);
        return instance;
    }
    dispose() {
        for (let instance of this.disposableInstance) {
            instance.dispose();
        }
    }
}
exports.Context = Context;
// export interface PrismResponse<T> extends HttpError, HttpSuccess<T> {  }
// export type PrismResponse<T> = HttpError | HttpSuccess<T>;
// export interface PrismResponse<T> implements HttpSuccess<T>;
class PrismResponse {
    constructor(a, b) {
        // if (a) return new HttpError(a.code, a.message);
        // if (b) return new HttpSuccess(b.code, b.reference, b.message, b.data);
        if (a) {
            this.code = a.code;
            this.error = true;
            this.success = undefined;
            this.message = a.message;
            this.token = a.token;
        }
        if (b) {
            this.code = b.code;
            this.error = undefined;
            this.success = true;
            this.message = b.message;
            this.data = b.data;
            this.reference = b.reference;
            this.token = b.token;
        }
    }
}
exports.PrismResponse = PrismResponse;
class HttpError {
    constructor(context, code, messageOrError) {
        this.success = undefined;
        this.error = true;
        this.code = code;
        this.token = credentials_1.Bearer.GetToken(context.request);
        if (messageOrError != undefined) {
            if (messageOrError instanceof Error) {
                this.message = messageOrError.message;
            }
            else if (util_1.isObject(messageOrError)) {
                this.message = JSON.stringify(messageOrError);
            }
            else {
                this.message = messageOrError.toString();
            }
        }
        else {
            this.message = "";
        }
    }
}
class HttpSuccess {
    constructor(code, token, data, message, reference) {
        this.success = true;
        this.error = undefined;
        this.code = code;
        this.token = token;
        this.data = data;
        this.message = message;
        this.reference = reference;
    }
}
class _Success extends HttpSuccess {
    constructor(context, data = {}, message = undefined, reference = undefined) {
        if (context && context.response)
            context.response.status(HttpStatusCode.OK);
        let token = undefined;
        if (context && context.request)
            token = credentials_1.Bearer.GetToken(context.request);
        super(HttpStatusCode.OK, token, data, message, reference);
    }
}
var HttpStatusCode;
(function (HttpStatusCode) {
    HttpStatusCode[HttpStatusCode["OK"] = 200] = "OK";
    HttpStatusCode[HttpStatusCode["Created"] = 201] = "Created";
    HttpStatusCode[HttpStatusCode["BadRequest"] = 400] = "BadRequest";
    HttpStatusCode[HttpStatusCode["Unauthorized"] = 401] = "Unauthorized";
    HttpStatusCode[HttpStatusCode["Forbidden"] = 403] = "Forbidden";
    HttpStatusCode[HttpStatusCode["NotFound"] = 404] = "NotFound";
    HttpStatusCode[HttpStatusCode["UnprocessableEntity"] = 422] = "UnprocessableEntity";
    HttpStatusCode[HttpStatusCode["InternalServerError"] = 500] = "InternalServerError";
    HttpStatusCode[HttpStatusCode["TooManyRequests"] = 429] = "TooManyRequests";
})(HttpStatusCode = exports.HttpStatusCode || (exports.HttpStatusCode = {}));
class _TooManyRequests extends HttpError {
    constructor(context, messageOrError = "Too many requests") {
        if (context && context.response)
            context.response.status(HttpStatusCode.TooManyRequests);
        super(context, HttpStatusCode.TooManyRequests, messageOrError);
    }
}
class _BadRequest extends HttpError {
    constructor(context, messageOrError = "Bad request") {
        if (context && context.response)
            context.response.status(HttpStatusCode.BadRequest);
        super(context, HttpStatusCode.BadRequest, messageOrError);
    }
}
class _Unauthorized extends HttpError {
    constructor(context, messageOrError = "Unauthorized") {
        if (context && context.response)
            context.response.status(HttpStatusCode.Unauthorized);
        super(context, HttpStatusCode.Unauthorized, messageOrError);
    }
}
class _Forbidden extends HttpError {
    constructor(context, messageOrError = "Forbidden") {
        if (context && context.response)
            context.response.status(HttpStatusCode.Forbidden);
        super(context, HttpStatusCode.Forbidden, messageOrError);
    }
}
class _NotFound extends HttpError {
    constructor(context, messageOrError = "Not found") {
        if (context && context.response)
            context.response.status(HttpStatusCode.NotFound);
        super(context, HttpStatusCode.NotFound, messageOrError);
    }
}
class _InternalServerError extends HttpError {
    constructor(context, messageOrError = "Internal server error") {
        if (context && context.response)
            context.response.status(HttpStatusCode.InternalServerError);
        super(context, HttpStatusCode.InternalServerError, messageOrError);
    }
}
class ResponsesClass {
    TooManyRequests(context, messageOrError) {
        return new PrismResponse(new _TooManyRequests(context, messageOrError), null);
    }
    BadRequest(context, messageOrError) {
        return new PrismResponse(new _BadRequest(context, messageOrError), null);
    }
    Unauthorized(context, messageOrError) {
        return new PrismResponse(new _Unauthorized(context, messageOrError), null);
    }
    NotFound(context, messageOrError) {
        return new PrismResponse(new _NotFound(context, messageOrError), null);
    }
    Forbidden(context, messageOrError) {
        return new PrismResponse(new _Forbidden(context, messageOrError), null);
    }
    InternalServerError(context, messageOrError) {
        return new PrismResponse(new _InternalServerError(context, messageOrError), null);
    }
    Success(context, data, message, reference) {
        return new PrismResponse(null, new _Success(context, data, message, reference));
    }
    OK(context, data, message, reference) {
        return new PrismResponse(null, new _Success(context, data, message, reference));
    }
}
exports.Responses = new ResponsesClass();
class FileServe {
    constructor(file, filename, extension, forceAttachment) {
        this.file = file;
        this.filename =
            filename != undefined
                ? filename
                : typeof file === "string"
                    ? Path.basename(file)
                    : "file";
        extension = extension != undefined ? extension : this.filename;
        this.contentType = require("mime-types").contentType(extension);
        this.forceAttachment = !!forceAttachment;
    }
    sendHeaders(res) {
        res.setHeader("Accept-Ranges", "bytes");
        res.setHeader("Content-Type", this.contentType);
        res.setHeader("Content-Disposition", `${this.forceAttachment ? "attachment" : "inline"}; filename=${this.filename}`);
    }
}
exports.FileServe = FileServe;
/**************************************************
 * Class Decorators
 **************************************************/
function Controller(mountpoint, path) {
    return (ctr) => {
        let c = exports.globalCtrState.getOrInsertController(ctr);
        c.explicitlyDeclared = true;
        if (mountpoint !== undefined) {
            if (typeof mountpoint === "string") {
                c.path = mountpoint;
            }
            else {
                exports.globalCtrState.registerMountpoint(mountpoint, ctr);
                c.path = typeof path === "string" ? path : ctr.name;
            }
        }
        else {
            c.path = ctr.name;
        }
        c.path = U.UrlJoin("/", c.path);
    };
}
exports.Controller = Controller;
/**
 * Adds express middleware to run before mounting the controller
 * @param { Express.RequestHandler[] } middleware - Array of middleware to add.
 */
function ControllerMiddleware(...middleware) {
    return (ctr) => {
        if (middleware != undefined) {
            let c = exports.globalCtrState.getOrInsertController(ctr);
            c.middleware = middleware.concat(c.middleware);
        }
    };
}
exports.ControllerMiddleware = ControllerMiddleware;
/**
 * @param { boolean } condition - Only mounts this controller if condition is true.
 */
function MountCondition(condition) {
    return (ctr) => {
        let c = exports.globalCtrState.getOrInsertController(ctr);
        c.mountCondition = c.mountCondition && condition;
    };
}
exports.MountCondition = MountCondition;
/**
 *  Only mounts this controller if NODE_ENV is set to "development"
 */
function Dev() {
    return MountCondition(process.env.NODE_ENV === "development");
}
exports.Dev = Dev;
/**
 *  Attach a documentation string to the controller
 *  @param {string} docStr - The documentation string.
 */
function DocController(docStr) {
    return (ctr) => {
        exports.globalCtrState.getOrInsertController(ctr).docString = docStr;
    };
}
exports.DocController = DocController;
/**
 * Generate test runner paths inside this controller
 */
function TestRunner() {
    return (ctr) => {
        exports.globalCtrState.getOrInsertController(ctr).generateTestRunnerPaths = true;
    };
}
exports.TestRunner = TestRunner;
/**************************************************
 * Method Decorators
 **************************************************/
function Method(method, path) {
    return function (target, propertyKey, descriptor) {
        path = path != undefined ? path : propertyKey;
        method = method.toLowerCase();
        let m = exports.globalCtrState
            .getOrInsertController(target.constructor)
            .getOrInsertMethod(propertyKey);
        m.methodMountpoints.push({
            path: U.UrlJoin("/", path),
            httpMethod: method,
        });
        m.explicitlyDeclared = true;
    };
}
exports.Method = Method;
function GET(path) {
    return Method("get", path);
}
exports.GET = GET;
function POST(path) {
    return Method("post", path);
}
exports.POST = POST;
function PUT(path) {
    return Method("put", path);
}
exports.PUT = PUT;
function PATCH(path) {
    return Method("patch", path);
}
exports.PATCH = PATCH;
function DELETE(path) {
    return Method("delete", path);
}
exports.DELETE = DELETE;
// export function Upload(
// 	path?: string
// ): (a: any, s: string, pd: PropertyDescriptor) => void {
// 	return Method("post", path);
// }
/**
 * Adds express middleware to run before the method
 * @param { Express.RequestHandler[] } middleware - Array of middleware to add.
 */
function ActionMiddleware(...middleware) {
    return function (target, propertyKey, descriptor) {
        if (middleware != undefined) {
            let m = exports.globalCtrState
                .getOrInsertController(target.constructor)
                .getOrInsertMethod(propertyKey);
            m.middleware = middleware.concat(m.middleware);
        }
    };
}
exports.ActionMiddleware = ActionMiddleware;
/**
 * Flags the method as "Express Compatible" and thus will be called with parameters (req,res,next)
 */
function ExpressCompatible() {
    return function (target, propertyKey, descriptor) {
        let m = exports.globalCtrState
            .getOrInsertController(target.constructor)
            .getOrInsertMethod(propertyKey);
        m.expressCompatible = true;
    };
}
exports.ExpressCompatible = ExpressCompatible;
/**
 *  Attach a documentation string to the method
 *  @param {string} docStr - The documentation string.
 */
function DocAction(docStr) {
    return function (target, propertyKey, descriptor) {
        let m = exports.globalCtrState
            .getOrInsertController(target.constructor)
            .getOrInsertMethod(propertyKey);
        m.docString = docStr;
    };
}
exports.DocAction = DocAction;
function MapParameterToRequestValue(rvc, valueKey, openApiType, modFunction) {
    return function (target, propertyKey, parameterIndex) {
        let m = exports.globalCtrState
            .getOrInsertController(target.constructor)
            .getOrInsertMethod(propertyKey);
        m.extraParametersMappings[parameterIndex] = {
            rvc: rvc,
            valueKey: valueKey,
            openApiType: openApiType,
            modFunction: modFunction,
        };
    };
}
exports.MapParameterToRequestValue = MapParameterToRequestValue;
function FromBody(valueKey, modFunction, openApiType) {
    return MapParameterToRequestValue("body", valueKey, openApiType, modFunction);
}
exports.FromBody = FromBody;
function FromQuery(valueKey, modFunction, openApiType) {
    return MapParameterToRequestValue("query", valueKey, openApiType, modFunction);
}
exports.FromQuery = FromQuery;
function FromPath(valueKey, modFunction, openApiType) {
    return MapParameterToRequestValue("path", valueKey, openApiType, modFunction);
}
exports.FromPath = FromPath;
function FromHeader(valueKey, modFunction, openApiType) {
    return MapParameterToRequestValue("header", valueKey, openApiType, modFunction);
}
exports.FromHeader = FromHeader;
function FromCookie(valueKey, modFunction, openApiType) {
    return MapParameterToRequestValue("cookie", valueKey, openApiType, modFunction);
}
exports.FromCookie = FromCookie;
/*********************************************************
 * Utils
 *********************************************************/
function DumpInternals() {
    for (let ck in exports.globalCtrState.controllers) {
        console.log("============================================");
        console.log(`Controller on path ${exports.globalCtrState.controllers[ck].path} built from class ${exports.globalCtrState.controllers[ck].ctr.name}`);
        console.log("Methods:");
        for (let mk in exports.globalCtrState.controllers[ck].methods) {
            let m = exports.globalCtrState.controllers[ck].methods[mk];
            console.log(`== ${mk} ==`);
            console.log(m);
            console.log("");
        }
    }
}
exports.DumpInternals = DumpInternals;
class PrismMethodOpenApiResponses {
}
exports.PrismMethodOpenApiResponses = PrismMethodOpenApiResponses;
class PrismMethod {
    constructor() {
        this.docString = "";
        this.expressCompatible = false;
        this.explicitlyDeclared = false;
        this.middleware = [];
        this.methodMountpoints = [];
        this.openApiResponses = {};
        this.extraParametersMappings = [];
    }
}
exports.PrismMethod = PrismMethod;
class PrismController {
    constructor() {
        this.docString = "";
        this.mountCondition = true;
        this.generateTestRunnerPaths = false;
        this.childController = false;
        this.explicitlyDeclared = false;
        this.methods = {};
        this.middleware = [];
        this.node = undefined;
    }
    getOrInsertMethod(key) {
        if (this.methods[key] === undefined) {
            this.methods[key] = new PrismMethod();
        }
        return this.methods[key];
    }
}
exports.PrismController = PrismController;
class PrismControllerTreeNode {
    constructor(controller) {
        this.children = [];
        this.controller = controller;
        this.fullPath = controller.path;
    }
}
exports.PrismControllerTreeNode = PrismControllerTreeNode;
class PrismControllerState {
    constructor() {
        this.mountpoints = [];
        this.controllers = {};
        this.controllersTree = [];
    }
    getController(ctr) {
        return this.controllers[ctr.toString()];
    }
    getOrInsertController(ctr) {
        let key = ctr.toString();
        if (this.controllers[key] === undefined) {
            this.controllers[key] = new PrismController();
            this.controllers[key].ctr = ctr;
        }
        return this.controllers[key];
    }
    registerMountpoint(dstCtr, ctr) {
        this.getOrInsertController(ctr).childController = true;
        this.mountpoints.push({ dstCtr: dstCtr, ctr: ctr });
    }
}
exports.PrismControllerState = PrismControllerState;
exports.globalCtrState = new PrismControllerState();
function addChildrenToTreeNode(node) {
    node.controller.node = node;
    for (let mp of exports.globalCtrState.mountpoints) {
        if (node.controller.ctr.toString() === mp.dstCtr.toString()) {
            let child = new PrismControllerTreeNode(exports.globalCtrState.getController(mp.ctr));
            addChildrenToTreeNode(child);
            node.children.push(child);
        }
    }
}
function buildControllersTree() {
    for (let ck in exports.globalCtrState.controllers) {
        let c = exports.globalCtrState.controllers[ck];
        if (c.childController === false) {
            let node = new PrismControllerTreeNode(c);
            addChildrenToTreeNode(node);
            exports.globalCtrState.controllersTree.push(node);
        }
    }
}
function indexAutogenerator(controller, children) {
    return (req, res) => {
        let content = `<html><head><style>${exports.docCss}</style></head><body>`;
        if (children && children.length) {
            content += `<div class="docbox">`;
            let i = 0;
            for (let child of children) {
                content += `<div><a class="mono" href=".${child.controller.path}/">${child.controller.path}</a></div>`;
                content += `<div style="color:#777;font-weight:400;">${child.controller.docString}</div>`;
                if (i < children.length - 1) {
                    content += `<hr />`;
                }
                i++;
            }
            content += `</div>`;
        }
        if (controller !== undefined) {
            content += `<div class="docbox">`;
            content += `<div>Viewing routes for controller <strong>${controller.ctr.name}</strong>:</div><br />`;
            let ml = Object.keys(controller.methods);
            let i = 0;
            for (let mk in controller.methods) {
                content += `<div style="line-height:18px;">`;
                for (let mmp of controller.methods[mk].methodMountpoints) {
                    content += `<span class="mono" style="color:#777;width:40px;display:inline-block;">${mmp.httpMethod.toUpperCase()}</span>`;
                    content += `<a href=".${mmp.path}/" class="mono">${controller.path}${mmp.path}</a>`;
                    content += `<div style="color:#777;margin-left:40px;">${controller.methods[mk].docString}</div>`;
                    if (i < ml.length - 1) {
                        content += `<hr />`;
                    }
                    i++;
                }
                content += `</div>`;
            }
            content += `</div>`;
        }
        content += "</body></html>";
        res.send(content);
    };
}
function mountMethod(controller, instance, methodKey) {
    let method = controller.methods[methodKey];
    if (method.explicitlyDeclared === false) {
        U.defaultWarnLogger(`Method "${methodKey}" was not explicitly declared with an HTTP request type decorator (e.g. @Prism.GET()). Defaulting to GET /${methodKey}`);
        method.methodMountpoints.push({
            path: `/${methodKey}`,
            httpMethod: "get",
        });
    }
    for (let mp of method.methodMountpoints) {
        let callback = (req, res, next) => {
            let context = new Context();
            let runner = () => __awaiter(this, void 0, void 0, function* () {
                let ret;
                if (method.expressCompatible) {
                    ret = instance[methodKey](req, res, next);
                }
                else {
                    context.request = req;
                    context.response = res;
                    context.nextMiddleware = next;
                    let params = [context];
                    if (method.extraParametersMappings[0] !== undefined) {
                        throw new Error("Cannot map first parameter, it should always be Context");
                    }
                    for (let i = 1; i < method.extraParametersMappings.length; i++) {
                        let mp = method.extraParametersMappings[i];
                        if (mp === undefined) {
                            params.push(undefined);
                        }
                        else {
                            switch (mp.rvc) {
                                case "body":
                                    if (mp.valueKey === undefined ||
                                        mp.valueKey === "") {
                                        params.push(req.body);
                                    }
                                    else {
                                        params.push(req.body[mp.valueKey]);
                                    }
                                    break;
                                case "query":
                                    params.push(req.query[mp.valueKey]);
                                    break;
                                case "path":
                                    params.push(req.params[mp.valueKey]);
                                    break;
                                case "header":
                                    params.push(req.headers[mp.valueKey]);
                                    break;
                                case "cookie":
                                    params.push(req.cookies[mp.valueKey]);
                                    break;
                            }
                            // TODO: Maybe instead of throwing, send a BadRequest response.
                            if (mp.modFunction) {
                                try {
                                    let x = mp.modFunction(params[params.length - 1]);
                                    params[params.length - 1] = x;
                                }
                                catch (e) {
                                    throw new Error(`Failed to execution modFunction (${mp.modFunction}) on parameter (${mp.valueKey}: ${params[params.length - 1]})`);
                                }
                            }
                        }
                    }
                    ret = instance[methodKey](...params);
                }
                if (ret instanceof Promise) {
                    ret = yield ret;
                }
                if (ret instanceof FileServe) {
                    // serveFile(res, ret);
                }
                else if (ret instanceof Object) {
                    if (ret.$render_view !== undefined) {
                        res.render(ret.$render_view, ret);
                    }
                    else {
                        res.json(ret);
                    }
                }
                else if (typeof ret === "string") {
                    res.send(ret);
                }
            });
            runner()
                .then(() => {
                context.dispose();
            })
                .catch((err) => {
                context.dispose();
                next(err);
            });
        };
        // TODO: Add throttle stuff to middleware
        controller.router[mp.httpMethod](U.UrlJoin(mp.path, "/"), ...method.middleware, callback);
    }
}
function useRouterAtPathStrict(baseRouter, basePath, router) {
    if (basePath.substring(basePath.length - 1) === "/") {
        basePath = basePath.trim().substr(0, basePath.length - 1);
    }
    let strictPath = U.UrlJoin(basePath, "/");
    if (strictPath !== "/") {
        baseRouter.use(strictPath, (req, res, next) => {
            if (req.originalUrl.substring(req.originalUrl.length - basePath.length) === basePath) {
                res.redirect(strictPath);
            }
            else {
                next();
            }
        }, router);
    }
    else {
        baseRouter.use(strictPath, router);
    }
}
function createRouterRecursive(app, controllerNode) {
    let controller = controllerNode.controller;
    controllerNode.fullPath = controller.path;
    if (controller.mountCondition === false) {
        return undefined;
    }
    if (controller.explicitlyDeclared === false) {
        U.defaultWarnLogger(`Controller "${controller.ctr.name}" was not explicitly declared with a @Controller decorator.`);
    }
    let instance = Reflect.construct(controller.ctr, []);
    controller.router = Express.Router({ mergeParams: true });
    for (let middleware of controller.middleware) {
        controller.router.use(middleware);
    }
    for (let mk in controller.methods) {
        mountMethod(controller, instance, mk);
    }
    for (let child of controllerNode.children) {
        let nc = createRouterRecursive(app, child);
        if (nc !== undefined) {
            useRouterAtPathStrict(controller.router, nc.path, nc.router);
            child.fullPath = U.UrlJoin(controllerNode.fullPath, "/", child.fullPath);
        }
    }
    if (controller.generateTestRunnerPaths) {
        T.injectTestRunnerMiddleware(controller);
    }
    if (process.env.NODE_ENV === "development") {
        controller.router.get("/", indexAutogenerator(controller, controllerNode.children));
    }
    return controller;
}
function handleRequestErrorMiddleware(err, req, res, next) {
    for (let i = 0; i < U.errorHandlers.length - 1; i++) {
        U.errorHandlers[i](err, req, res, U.errorHandlers[i + 1]);
    }
    if (U.errorHandlers.length > 0) {
        U.errorHandlers[U.errorHandlers.length - 1](err, req, res, onRequestError);
    }
    else {
        onRequestError(err, req, res, next);
    }
}
function onRequestError(err, req, res, next) {
    let c = new Context();
    c.request = req;
    c.response = res;
    c.nextMiddleware = next;
    if (err.name === "UnauthorizedError") {
        res.sendStatus(401);
    }
    else {
        if (process.env.NODE_ENV === "development") {
            res.statusCode = 500;
            if (err instanceof HttpError) {
                U.defaultErrorLogger(err);
                // res.status(err.code).send(err.message);
                res.json(new PrismResponse(new _InternalServerError(c, err.message), null));
            }
            else if (err instanceof Error) {
                U.defaultErrorLogger({
                    name: err.name,
                    message: err.message,
                    stack: err.stack,
                });
                // An unexpected error probably happened.
                // Send an InternalServerError HttpError as a PrismResponse.
                res.json(new PrismResponse(new _InternalServerError(c, err.message), null));
                // res.json({
                // 	code: HttpStatusCode.InternalServerError,
                // 	error: true,
                // 	success: false,
                // 	name: err.name,
                // 	message: err.message,
                // });
            }
            else {
                U.defaultErrorLogger(err);
                // res.json(err);
                res.json(new PrismResponse(new _InternalServerError(c, err), null));
            }
        }
        else {
            res.sendStatus(500);
        }
    }
}
function onRequestNotFound(req, res, next) {
    let c = new Context();
    c.request = req;
    c.response = res;
    c.nextMiddleware = next;
    res.status(404).send(new PrismResponse(new _NotFound(c, "Route not defined."), null));
    // res.sendStatus(500);
}
exports.initialized = false;
function initialize(app, ...requiredDirectories) {
    initializeAtRoute("/", app, ...requiredDirectories);
}
exports.initialize = initialize;
function initializeAtRoute(rootPath, app, ...requiredDirectories) {
    let implicitTests = false;
    let implicitControllers = false;
    if (!requiredDirectories.find((p) => {
        return p === "tests";
    })) {
        requiredDirectories.push("tests");
        implicitTests = true;
    }
    if (!requiredDirectories.find((p) => {
        return p === "controllers";
    })) {
        requiredDirectories.push("controllers");
        implicitControllers = true;
    }
    for (let requiredDirectory of requiredDirectories) {
        let path = "";
        if (Path.isAbsolute(requiredDirectory)) {
            path = requiredDirectory;
        }
        else {
            path = Path.join(process.cwd(), requiredDirectory);
        }
        try {
            // U.defaultInfoLogger("Loading components from: " + path);
            FS.accessSync(path);
        }
        catch (err) {
            if ((requiredDirectory !== "controllers" || !implicitControllers) &&
                (requiredDirectory !== "tests" || !implicitTests)) {
                U.defaultWarnLogger("Cannot access path: " + path);
            }
            continue;
        }
        require("require-all")(path);
    }
    rootPath = rootPath || "/";
    buildControllersTree();
    for (let node of exports.globalCtrState.controllersTree) {
        let nc = createRouterRecursive(app, node);
        if (nc !== undefined) {
            useRouterAtPathStrict(app, U.UrlJoin(rootPath, nc.path), nc.router);
            node.fullPath = U.UrlJoin(rootPath, "/", node.fullPath);
        }
    }
    if (process.env.NODE_ENV === "development") {
        app.get(rootPath, indexAutogenerator(undefined, exports.globalCtrState.controllersTree));
    }
    app.use(handleRequestErrorMiddleware);
    app.use(onRequestNotFound);
    exports.initialized = true;
}
exports.initializeAtRoute = initializeAtRoute;
let PrismInternalDevelopmentController = class PrismInternalDevelopmentController {
    index() {
        return documentation_1.getDocsAsHTML();
    }
};
__decorate([
    GET("/"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], PrismInternalDevelopmentController.prototype, "index", null);
PrismInternalDevelopmentController = __decorate([
    Dev(),
    Controller("/__dev"),
    DocController("This development API reference controller is only mounted when process.NODE_ENV is 'development'.")
], PrismInternalDevelopmentController);
let PrismInternalTestController = class PrismInternalTestController {
};
PrismInternalTestController = __decorate([
    Dev(),
    TestRunner(),
    Controller("/__test"),
    DocController("This interactive test runner is only mounted when process.NODE_ENV is 'development'.")
], PrismInternalTestController);
//# sourceMappingURL=controller.js.map