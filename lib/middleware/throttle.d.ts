import * as Express from "express";
/**
 *
 * @param maxPerSecond: Maximum number of requests allowed by an IP per second.
 * @returns
 */
export declare function Throttle(maxPerSecond: any): Express.Handler;
