"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Status = void 0;
var Status;
(function (Status) {
    Status[Status["ONLINE"] = 3] = "ONLINE";
    Status[Status["CONNECTING"] = 2] = "CONNECTING";
    Status[Status["CLOSED"] = 1] = "CLOSED";
    Status[Status["OFFLINE"] = 0] = "OFFLINE";
})(Status = exports.Status || (exports.Status = {}));
;
//# sourceMappingURL=status.js.map