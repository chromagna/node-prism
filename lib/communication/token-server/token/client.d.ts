/// <reference types="node" />
import * as net from "net";
import * as tls from "tls";
import { Connection } from "../connection/connection";
import { TokenAPI } from "./api";
import { SecurityOpts } from "./server";
export declare type TokenClientOptions = tls.ConnectionOptions & net.SocketConstructorOpts & SecurityOpts;
export declare interface TokenClient extends TokenAPI {
    on(event: "remoteClose", listener: (connection: Connection) => void): this;
    on(event: string, listener: (...args: any[]) => void): this;
    once(event: "remoteClose", listener: (connection: Connection) => void): this;
    once(event: string, listener: (...args: any[]) => void): this;
    emit(event: "remoteClose", connection: Connection): boolean;
    emit(event: string, ...args: any[]): boolean;
}
export declare class TokenClient extends TokenAPI {
    private options;
    private socket;
    private connection;
    private hadError;
    constructor(options: TokenClientOptions);
    close(): boolean;
    connect(): boolean;
    send(token: Buffer): boolean;
    private applyListeners;
    private updateConnection;
}
