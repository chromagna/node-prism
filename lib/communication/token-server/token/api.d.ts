/// <reference types="node" />
import { EventEmitter } from "events";
import { Status } from "./status";
import { Connection } from "../connection/connection";
export declare type ErrorListener = (error: Error) => void;
export declare type ConnectListener = () => void;
export declare type CloseListener = (hadError: boolean) => void;
export declare type TokenListener = (token: Buffer, connection: Connection) => void;
export declare interface TokenAPI extends EventEmitter {
    on(event: "error", listener: ErrorListener): this;
    on(event: "connect", listener: ConnectListener): this;
    on(event: "close", listener: CloseListener): this;
    on(event: "token", listener: TokenListener): this;
    once(event: "error", listener: ErrorListener): this;
    once(event: "connect", listener: ConnectListener): this;
    once(event: "close", listener: CloseListener): this;
    once(event: "token", listener: TokenListener): this;
    emit(event: "error", Error: any): boolean;
    emit(event: "connect"): boolean;
    emit(event: "close", hadError: boolean): boolean;
    emit(event: "token", token: Buffer, connection: Connection): boolean;
}
export declare abstract class TokenAPI extends EventEmitter {
    status: Status;
    abstract close(): boolean;
    abstract connect(delay: number): boolean;
}
