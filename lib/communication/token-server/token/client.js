"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenClient = void 0;
const net = require("net");
const tls = require("tls");
const connection_1 = require("../connection/connection");
const api_1 = require("./api");
const status_1 = require("./status");
class TokenClient extends api_1.TokenAPI {
    constructor(options) {
        super();
        this.connection = null;
        this.options = options;
        this.connect();
    }
    close() {
        if (this.status <= status_1.Status.CLOSED)
            return false;
        this.status = status_1.Status.CLOSED;
        this.socket.end();
        this.connection = null;
        return true;
    }
    connect() {
        if (this.status >= status_1.Status.CONNECTING)
            return false;
        this.hadError = false;
        this.status = status_1.Status.CONNECTING;
        if (this.options.useInsecure)
            this.socket = net.connect(this.options);
        else
            this.socket = tls.connect(this.options);
        this.connection = null;
        this.applyListeners();
        return true;
    }
    send(token) {
        if (this.connection)
            return this.connection.send(token);
        return false;
    }
    applyListeners() {
        this.socket.on("error", (error) => {
            this.hadError = true;
            this.emit("error", error);
        });
        this.socket.on("close", () => {
            this.status = status_1.Status.OFFLINE;
            this.emit("close", this.hadError);
        });
        this.socket.on(`${this.options.useInsecure ? "connect" : "secureConnect"}`, () => {
            this.updateConnection();
            this.status = status_1.Status.ONLINE;
            this.emit("connect");
        });
    }
    updateConnection() {
        const connection = new connection_1.Connection(this.socket);
        connection.on("token", (token) => {
            this.emit("token", token, connection);
        });
        connection.on("remoteClose", () => this.emit("remoteClose", connection));
        this.connection = connection;
    }
}
exports.TokenClient = TokenClient;
//# sourceMappingURL=client.js.map