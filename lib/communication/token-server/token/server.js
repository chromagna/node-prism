"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenServer = void 0;
const net = require("net");
const tls = require("tls");
const connection_1 = require("../connection/connection");
const api_1 = require("./api");
const status_1 = require("./status");
class TokenServer extends api_1.TokenAPI {
    constructor(options) {
        super();
        this.connections = [];
        this.options = options;
        const self = this;
        if (this.options.useInsecure) {
            this.server = net.createServer(this.options, function (socket) {
                var _a;
                self.options.keepAlive && socket.setKeepAlive(true, (_a = self.options.keepAliveInitialDelay) !== null && _a !== void 0 ? _a : 60000);
                socket.on("error", err => this.emit("clientError", err));
            });
        }
        else {
            this.server = tls.createServer(this.options, function (socket) {
                var _a;
                self.options.keepAlive && socket.setKeepAlive(true, (_a = self.options.keepAliveInitialDelay) !== null && _a !== void 0 ? _a : 60000);
                socket.on("error", err => this.emit("clientError", err));
            });
        }
        this.applyListeners();
        this.connect();
    }
    // TODO: When client emits close - does this actually get received?
    close() {
        if (!this.server.listening)
            return false;
        this.status = status_1.Status.CLOSED;
        this.server.close();
        for (const connection of this.connections)
            connection.remoteClose();
        return true;
    }
    connect() {
        if (this.status >= status_1.Status.CONNECTING)
            return false;
        this.hadError = false;
        this.status = status_1.Status.CONNECTING;
        this.server.listen(this.options);
        return true;
    }
    applyListeners() {
        this.server.on("listening", () => {
            this.status = status_1.Status.ONLINE;
            this.emit("connect");
        });
        this.server.on("tlsClientError", (error) => { });
        this.server.on("clientError", (error) => { });
        this.server.on("error", (error) => {
            this.hadError = true;
            this.emit("error", error);
            this.server.close();
        });
        this.server.on("close", () => {
            this.status = status_1.Status.OFFLINE;
            this.emit("close", this.hadError);
        });
        this.server.on(`${this.options.useInsecure ? "connection" : "secureConnection"}`, (socket) => {
            const connection = new connection_1.Connection(socket);
            this.connections.push(connection);
            connection.once("close", () => {
                const i = this.connections.indexOf(connection);
                this.connections.splice(i, 1);
            });
            connection.on("token", (token) => {
                this.emit("token", token, connection);
            });
        });
    }
}
exports.TokenServer = TokenServer;
//# sourceMappingURL=server.js.map