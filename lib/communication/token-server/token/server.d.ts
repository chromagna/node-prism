/// <reference types="node" />
import * as net from "net";
import * as tls from "tls";
import { Connection } from "../connection/connection";
import { TokenAPI } from "./api";
export interface SecurityOpts {
    useInsecure?: boolean;
    keepAlive?: boolean;
    keepAliveInitialDelay?: number;
}
export declare type TokenServerOptions = tls.TlsOptions & net.ListenOptions & net.SocketConstructorOpts & SecurityOpts;
export declare class TokenServer extends TokenAPI {
    connections: Array<Connection>;
    private options;
    private server;
    private hadError;
    constructor(options: TokenServerOptions);
    close(): boolean;
    connect(): boolean;
    private applyListeners;
}
