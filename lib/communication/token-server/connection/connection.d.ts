/// <reference types="node" />
import { EventEmitter } from "events";
import { Duplex } from "stream";
export declare interface Connection extends EventEmitter {
    on(event: "token", handler: (token: Buffer) => void): this;
    on(event: "remoteClose", handler: () => void): this;
    on(event: "close", handler: () => void): this;
    once(event: "token", handler: (token: Buffer) => void): this;
    once(event: "remoteClose", handler: () => void): this;
    once(event: "close", handler: () => void): this;
    emit(event: "token", token: Buffer): boolean;
    emit(event: "remoteClose"): boolean;
    emit(event: "close"): boolean;
}
export declare class Connection extends EventEmitter {
    socket: Duplex;
    private buffer;
    constructor(socket: Duplex);
    get isDead(): boolean;
    send(data: Buffer): boolean;
    close(): boolean;
    remoteClose(): boolean;
    private applyEvents;
    private parse;
}
