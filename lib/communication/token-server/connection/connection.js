"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Connection = void 0;
const events_1 = require("events");
const message_1 = require("./message");
const CLOSE_TOKEN = Buffer.from("\\\n");
class Connection extends events_1.EventEmitter {
    constructor(socket) {
        super();
        this.buffer = Buffer.allocUnsafe(0);
        this.socket = socket;
        this.applyEvents();
    }
    get isDead() {
        return !this.socket.writable;
    }
    send(data) {
        if (this.isDead)
            return false;
        this.socket.write(message_1.Message.escape(data));
        return true;
    }
    close() {
        if (this.isDead)
            return false;
        this.socket.end();
        return true;
    }
    remoteClose() {
        if (this.isDead)
            return false;
        this.socket.write(CLOSE_TOKEN);
        return true;
    }
    applyEvents() {
        this.socket.on("data", (data) => {
            this.buffer = Buffer.concat([this.buffer, data]);
            this.parse();
        });
        this.socket.on("close", () => this.emit("close"));
    }
    parse() {
        while (true) {
            const i = this.buffer.indexOf(message_1.NEWLINE);
            if (i === -1)
                break;
            const data = this.buffer.slice(0, i + 1); // + 1 to include separating newline
            if (data.equals(CLOSE_TOKEN))
                this.emit("remoteClose");
            else
                this.emit("token", message_1.Message.unescape(data));
            this.buffer = this.buffer.slice(i + 1);
        }
    }
}
exports.Connection = Connection;
//# sourceMappingURL=connection.js.map