"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class QueueItem {
    constructor(value, expiresIn) {
        this.value = value;
        this.expireTime = Date.now() + expiresIn;
    }
    get expiresIn() {
        return this.expireTime - Date.now();
    }
    get isExpired() {
        return Date.now() > this.expireTime;
    }
}
exports.default = QueueItem;
//# sourceMappingURL=queue-item.js.map