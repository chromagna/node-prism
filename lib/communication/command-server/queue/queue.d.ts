import QueueItem from "./queue-item";
export default class Queue<T> {
    private items;
    add(item: T, expiresIn: number): void;
    get isEmpty(): boolean;
    pop(): QueueItem<T> | null;
}
