"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const queue_item_1 = require("./queue-item");
class Queue {
    constructor() {
        this.items = [];
    }
    add(item, expiresIn) {
        this.items.push(new queue_item_1.default(item, expiresIn));
    }
    get isEmpty() {
        let i = this.items.length;
        while (i--) {
            const item = this.items[i];
            if (item.isExpired)
                this.items.splice(i, 1);
            else
                return false;
        }
        return true;
    }
    pop() {
        while (this.items.length) {
            const item = this.items.shift();
            if (!item.isExpired)
                return item;
        }
        return null;
    }
}
exports.default = Queue;
//# sourceMappingURL=queue.js.map