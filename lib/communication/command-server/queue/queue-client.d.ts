/// <reference types="node" />
import { TokenClient, TokenClientOptions } from "../../token-server/token/client";
export default class QueueClient extends TokenClient {
    private queue;
    constructor(options: TokenClientOptions);
    sendToken(token: Buffer, expiresIn: number): void;
    private applyEvents;
}
