"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("../../token-server/token/client");
const queue_1 = require("./queue");
class QueueClient extends client_1.TokenClient {
    constructor(options) {
        super(options);
        this.queue = new queue_1.default();
        this.applyEvents();
    }
    sendToken(token, expiresIn) {
        const success = this.send(token);
        if (!success)
            this.queue.add(token, expiresIn);
    }
    applyEvents() {
        this.on("connect", () => {
            while (!this.queue.isEmpty) {
                const item = this.queue.pop();
                this.sendToken(item.value, item.expiresIn);
            }
        });
    }
}
exports.default = QueueClient;
//# sourceMappingURL=queue-client.js.map