export default class QueueItem<T> {
    value: T;
    private expireTime;
    constructor(value: T, expiresIn: number);
    get expiresIn(): number;
    get isExpired(): boolean;
}
