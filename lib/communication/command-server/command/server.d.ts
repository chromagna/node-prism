import { Connection, TokenServer, TokenServerOptions } from "../../token-server";
export declare type CommandImplementation = (payload: any, connection: Connection) => Promise<any>;
export declare class CommandServer extends TokenServer {
    private commands;
    constructor(options: TokenServerOptions);
    command(command: number, fn: CommandImplementation): void;
    private initEvents;
    private runCommand;
}
