"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandClient = void 0;
const code_error_1 = require("../error/code-error");
const error_serializer_1 = require("../error/error-serializer");
const queue_client_1 = require("../queue/queue-client");
const command_1 = require("./command");
const id_manager_1 = require("./id-manager");
class CommandClient extends queue_client_1.default {
    constructor(options) {
        super(options);
        this.ids = new id_manager_1.default(Math.pow(2, 16) - 1);
        this.callbacks = {};
        this.initEvents();
    }
    command(command, payload, expiresIn) {
        return __awaiter(this, void 0, void 0, function* () {
            if (command === 255)
                throw new code_error_1.default(`Command 255 is reserved for errors.`, "ERESERVED", "CommandError");
            const id = this.ids.reserve();
            const token = command_1.Command.tokenize({ id, command, payload });
            this.sendToken(token, expiresIn);
            const response = this.createResponsePromise(id);
            const timeout = this.createTimeoutPromise(id, expiresIn);
            return Promise.race([response, timeout]);
        });
    }
    initEvents() {
        this.on("token", (token) => {
            try {
                const data = command_1.Command.parse(token);
                if (this.callbacks[data.id]) {
                    if (data.command === 255) {
                        const error = error_serializer_1.default.deserialize(data.payload);
                        this.callbacks[data.id](error);
                    }
                    else {
                        this.callbacks[data.id](null, data.payload);
                    }
                }
            }
            catch (error) {
                this.emit("error", error);
            }
        });
    }
    createTimeoutPromise(id, expiresIn) {
        return new Promise((_, reject) => {
            setTimeout(() => {
                this.ids.release(id);
                delete this.callbacks[id];
                reject(new code_error_1.default(`Command timed out after ${expiresIn} ms.`, "ETIMEOUT", "CommandError"));
            }, expiresIn);
        });
    }
    createResponsePromise(id) {
        return new Promise((resolve, reject) => {
            this.callbacks[id] = (error, result) => {
                this.ids.release(id);
                delete this.callbacks[id];
                if (error)
                    reject(error);
                else
                    resolve(result);
            };
        });
    }
}
exports.CommandClient = CommandClient;
//# sourceMappingURL=client.js.map