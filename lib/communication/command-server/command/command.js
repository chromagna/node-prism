"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Command = void 0;
class Command {
    static tokenize(command) {
        if (typeof command.payload === "undefined")
            throw new TypeError("Payload must not be undefined.");
        const payloadString = JSON.stringify(command.payload);
        const payload = Buffer.from(payloadString);
        const buffer = Buffer.allocUnsafe(payload.length + 3);
        buffer.writeUInt16LE(command.id, 0);
        buffer.writeUInt8(command.command, 2);
        payload.copy(buffer, 3);
        return buffer;
    }
    static parse(token) {
        if (token.length < 3)
            throw new TypeError(`Token too short: ${token}`);
        const id = token.readUInt16LE(0);
        const command = token.readUInt8(2);
        const payloadString = token.slice(3).toString();
        const payload = JSON.parse(payloadString);
        return {
            id, command, payload
        };
    }
}
exports.Command = Command;
//# sourceMappingURL=command.js.map