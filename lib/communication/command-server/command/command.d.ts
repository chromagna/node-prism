/// <reference types="node" />
export interface CommandData {
    id: number;
    command: number;
    payload: any;
}
export declare class Command {
    static tokenize(command: CommandData): Buffer;
    static parse(token: Buffer): CommandData;
}
