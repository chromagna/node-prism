import { TokenClientOptions } from "../../token-server";
import QueueClient from "../queue/queue-client";
export declare class CommandClient extends QueueClient {
    private ids;
    private callbacks;
    constructor(options: TokenClientOptions);
    command(command: number, payload: any, expiresIn: number): Promise<any>;
    private initEvents;
    private createTimeoutPromise;
    private createResponsePromise;
}
