export default class IdManager {
    ids: Array<true | false>;
    private index;
    private maxIndex;
    constructor(maxIndex: number);
    release(id: number): void;
    reserve(): number;
}
