"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandServer = void 0;
const token_server_1 = require("../../token-server");
const code_error_1 = require("../error/code-error");
const error_serializer_1 = require("../error/error-serializer");
const command_1 = require("./command");
class CommandServer extends token_server_1.TokenServer {
    constructor(options) {
        super(options);
        this.commands = {};
        this.initEvents();
    }
    command(command, fn) {
        this.commands[command] = fn;
    }
    initEvents() {
        this.on("token", (token, connection) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { id, command, payload } = command_1.Command.parse(token);
                this.runCommand(id, command, payload, connection);
            }
            catch (error) {
                this.emit("error", error);
            }
        }));
    }
    runCommand(id, command, payload, connection) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!this.commands[command])
                    throw new code_error_1.default(`Command [${command}] not found.`, "ENOTFOUND", "CommandError");
                const result = yield this.commands[command](payload, connection);
                connection.send(command_1.Command.tokenize({
                    command,
                    id,
                    payload: result,
                }));
            }
            catch (error) {
                const payload = error_serializer_1.default.serialize(error);
                connection.send(command_1.Command.tokenize({
                    command: 255,
                    id,
                    payload,
                }));
            }
        });
    }
}
exports.CommandServer = CommandServer;
//# sourceMappingURL=server.js.map