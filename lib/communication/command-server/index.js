"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandClient = exports.CommandServer = void 0;
var server_1 = require("./command/server");
Object.defineProperty(exports, "CommandServer", { enumerable: true, get: function () { return server_1.CommandServer; } });
var client_1 = require("./command/client");
Object.defineProperty(exports, "CommandClient", { enumerable: true, get: function () { return client_1.CommandClient; } });
//# sourceMappingURL=index.js.map