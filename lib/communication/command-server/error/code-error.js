"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CodeError extends Error {
    constructor(message, code, name) {
        super(message);
        if (typeof code === "string")
            this.code = code;
        if (typeof name === "string")
            this.name = name;
    }
}
exports.default = CodeError;
//# sourceMappingURL=code-error.js.map