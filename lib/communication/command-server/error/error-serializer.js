"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ErrorSerializer {
    static serialize(error) {
        const data = {
            message: error.message,
            name: error.name,
            stack: error.stack,
        };
        Object.assign(data, error);
        return data;
    }
    static deserialize(data) {
        const factory = this.getFactory(data);
        const error = new factory(data.message);
        Object.assign(error, data);
        return error;
    }
    static getFactory(data) {
        const name = data.name;
        if (name.endsWith("Error") && global[name])
            return global[name];
        return Error;
    }
}
exports.default = ErrorSerializer;
//# sourceMappingURL=error-serializer.js.map