import * as Express from "express";
interface GetTokenOptions {
    proxy?: boolean;
    header?: string;
}
export declare class Bearer {
    static GetToken(req: Express.Request, options?: GetTokenOptions): string | undefined;
}
export {};
