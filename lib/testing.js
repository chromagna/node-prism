"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.injectTestRunnerMiddleware = exports.PrismTestResult = exports.PrismTest = exports.PrismFixture = exports.PrismTestsState = exports.After = exports.Before = exports.Test = exports.Fixture = exports.globalTestState = void 0;
/**
 * This is a WIP.
 */
const Crypto = require("crypto");
const U = require("./utils");
/**
 * Registers a fixture of tests with the global test runner.
 * @param humanReadableName A human readable name.
 * @returns void
 */
function Fixture(humanReadableName) {
    return (ctr) => {
        let fixture = exports.globalTestState.getOrInsertFixture(ctr);
        fixture.humanReadableName = typeof (humanReadableName) === "string" ? humanReadableName : ctr.name;
        fixture.explicitlyDeclared = true;
    };
}
exports.Fixture = Fixture;
/**
 * Register a new test. If the test throws, it fails. Otherwise, it passes.
 * @param humanReadableName A human readable name.
 * @returns void
 */
function Test(humanReadableName) {
    return function (target, propertyKey, descriptor) {
        let f = exports.globalTestState.getOrInsertFixture(target.constructor);
        let t = f.getOrInsertTest(propertyKey);
        t.humanReadableName = humanReadableName;
        t.explicitlyDeclared = true;
    };
}
exports.Test = Test;
/**
 * Method to run before any of the tests.
 * @returns void
 */
function Before() {
    return function (target, propertyKey, descriptor) {
        exports.globalTestState.getOrInsertFixture(target.constructor).runBeforeMethods.push(propertyKey);
    };
}
exports.Before = Before;
/**
 * Method to run after any of the tests.
 * @returns void
 */
function After() {
    return function (target, propertyKey, descriptor) {
        exports.globalTestState.getOrInsertFixture(target.constructor).runAfterMethods.push(propertyKey);
    };
}
exports.After = After;
class PrismTestsState {
    constructor() {
        this.fixtures = {};
    }
    getOrInsertFixture(ctr) {
        let key = ctr.toString();
        if (this.fixtures[key] == undefined) {
            this.fixtures[key] = new PrismFixture();
            this.fixtures[key].ctr = ctr;
            this.fixtures[key].humanReadableName = this.fixtures[key].humanReadableName == undefined ? ctr.name : this.fixtures[key].humanReadableName;
        }
        return this.fixtures[key];
    }
    generateCompleteFixtureMetadata() {
        let metadata = {};
        for (let fx in this.fixtures) {
            let fixture = this.fixtures[fx];
            let fxHash = PrismTestsState.getFixtureHashId(fixture);
            metadata[fxHash] = {};
            for (let t in fixture.tests) {
                metadata[fxHash][t] = true;
            }
        }
        return metadata;
    }
    static getFixtureHashId(fixture) {
        let hasher = Crypto.createHash("sha256");
        hasher.update(fixture.ctr.toString());
        return fixture.ctr.name + "_" + hasher.digest("hex").substr(0, 8);
    }
    generateRainbowTables() {
        let ret = {};
        for (let fx in this.fixtures) {
            let fixture = this.fixtures[fx];
            let fxHash = PrismTestsState.getFixtureHashId(fixture);
            ret[fxHash] = fixture;
        }
        return ret;
    }
    run(testsToRun) {
        return __awaiter(this, void 0, void 0, function* () {
            if (testsToRun == undefined) {
                testsToRun = this.generateCompleteFixtureMetadata();
            }
            let testResults = [];
            let rainbowTables = this.generateRainbowTables();
            for (let fx in testsToRun) {
                let fixture = rainbowTables[fx];
                let fi = Reflect.construct(fixture.ctr, []);
                for (let mn of fixture.runBeforeMethods) {
                    let r = fi[mn]();
                    if (r instanceof Promise) {
                        yield r;
                    }
                }
                for (let mn in testsToRun[fx]) {
                    if (testsToRun[fx][mn] === true && fixture.tests[mn] != undefined && fi[mn] != undefined) {
                        let result = new PrismTestResult();
                        result.fixtureKey = fx;
                        result.fixtureDesc = fixture.humanReadableName;
                        result.testKey = mn;
                        result.testDesc = fixture.tests[mn].humanReadableName;
                        try {
                            let r = fi[mn]();
                            if (r instanceof Promise) {
                                yield r;
                            }
                            result.passed = true;
                            result.message = "";
                        }
                        catch (err) {
                            result.passed = false;
                            if (err instanceof Error) {
                                // result.message = JSON.stringify({ name: err.name, message: err.message, stack: err.stack });
                                result.message = { name: err.name, message: err.message, stack: err.stack };
                            }
                            else if (err instanceof Object) {
                                result.message = err;
                            }
                            else if (typeof (err) === "string") {
                                result.message = err;
                            }
                            else {
                                result.message = err.toString();
                            }
                        }
                        testResults.push(result);
                    }
                }
                for (let mn of fixture.runAfterMethods) {
                    let r = fi[mn]();
                    if (r instanceof Promise) {
                        yield r;
                    }
                }
            }
            return testResults;
        });
    }
}
exports.PrismTestsState = PrismTestsState;
class PrismFixture {
    constructor() {
        this.runBeforeMethods = [];
        this.runAfterMethods = [];
        this.tests = {};
        this.explicitlyDeclared = false;
    }
    getOrInsertTest(key) {
        if (this.tests[key] == undefined) {
            this.tests[key] = new PrismTest();
        }
        return this.tests[key];
    }
}
exports.PrismFixture = PrismFixture;
class PrismTest {
    constructor() {
        this.explicitlyDeclared = false;
    }
}
exports.PrismTest = PrismTest;
class PrismTestResult {
}
exports.PrismTestResult = PrismTestResult;
exports.globalTestState = new PrismTestsState();
function runSetOfTestsAndFillResponse(testsToRun, res) {
    return __awaiter(this, void 0, void 0, function* () {
        let results = yield exports.globalTestState.run(testsToRun);
        let failed = false;
        for (let result of results) {
            if (result.passed === false) {
                failed = true;
            }
        }
        res.statusCode = failed ? 418 : 200;
        res.json(results);
    });
}
let css = `body {
    font-family: Arial, sans-serif;
}
`;
function generateInteractiveTestRunnerMiddleware(useFixture) {
    return (req, res) => {
        let content = `
        <html><head><style>${css}</style></head><body>`;
        let useFixtureHash = undefined;
        if (useFixture == undefined) {
        }
        else {
            useFixtureHash = PrismTestsState.getFixtureHashId(useFixture);
            content += `<div>Use fixture: ${useFixtureHash}</div>`;
        }
        for (let fx in exports.globalTestState.fixtures) {
            let fixture = exports.globalTestState.fixtures[fx];
            let fxHash = PrismTestsState.getFixtureHashId(fixture);
            if (useFixture != undefined && fxHash !== useFixtureHash) {
                continue;
            }
            content += `<div id="${fxHash}-header">`;
            content += `<input class="runner" id="${fxHash}" type="checkbox" checked />`;
            content += `<a href="fixture/${fxHash}/">${fixture.humanReadableName}</a>`;
            content += `</div>`;
            for (let t in fixture.tests) {
                let test = fixture.tests[t];
                content += `<div style="margin-left:20px;">`;
                content += `<input class="${fxHash}" id="${t}" type="checkbox" checked />`;
                content += `${test.humanReadableName} <span id="${fxHash}_${t}_result"></span>`;
                content += `</div>`;
            }
        }
        content += `</body></html>`;
        res.send(content);
    };
}
function injectTestRunnerMiddleware(controller) {
    controller.router.get("/all", (req, res) => __awaiter(this, void 0, void 0, function* () {
        yield runSetOfTestsAndFillResponse(undefined, res);
    }));
    controller.router.get("/metadata", (req, res) => {
        res.json(exports.globalTestState.generateCompleteFixtureMetadata());
    });
    for (let fxk in exports.globalTestState.fixtures) {
        let fixture = exports.globalTestState.fixtures[fxk];
        let fxkHash = PrismTestsState.getFixtureHashId(fixture);
        controller.router.get(U.UrlJoin("/fixture/", fxkHash, "/"), generateInteractiveTestRunnerMiddleware(fixture));
        controller.router.get(U.UrlJoin("/fixture/", fxkHash, "/all"), (req, res) => __awaiter(this, void 0, void 0, function* () {
            let metadata = exports.globalTestState.generateCompleteFixtureMetadata();
            let thisFixtureSet = {};
            for (let mk in metadata) {
                if (mk === fxkHash) {
                    thisFixtureSet[mk] = metadata[mk];
                }
            }
            yield runSetOfTestsAndFillResponse(thisFixtureSet, res);
        }));
    }
    controller.router.get("/", generateInteractiveTestRunnerMiddleware());
}
exports.injectTestRunnerMiddleware = injectTestRunnerMiddleware;
//# sourceMappingURL=testing.js.map