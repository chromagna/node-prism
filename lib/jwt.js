"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Verify = exports.Decode = exports.Encode = exports.UrlEncodedToBase64 = exports.Base64ToUrlEncoded = exports.JsonBase64Decode = exports.JsonBase64Encode = exports.Algorithms = exports.IsValidAlgorithm = exports.ALGORITHMS = exports.IsVerifyValid = void 0;
const crypto = require("crypto");
function IsVerifyValid(opts, result) {
    const keys = Object.keys(opts);
    if (opts.sig === undefined) {
        keys.push("sig");
    }
    let as_any = result;
    for (let i = 0; i < keys.length; i++) {
        if (as_any[keys[i]] !== true) {
            return false;
        }
    }
    return true;
}
exports.IsVerifyValid = IsVerifyValid;
/// ALGORITHM ///
exports.ALGORITHMS = [
    "HS256",
    "HS384",
    "HS512",
    "RS256",
    "RS384",
    "RS512",
    /*
    "ES256",
    "ES384",
    "ES512",
    */
];
function IsValidAlgorithm(alg) {
    return exports.ALGORITHMS.includes(alg);
}
exports.IsValidAlgorithm = IsValidAlgorithm;
exports.Algorithms = {
    HS256: CreateHmacAlg(256),
    HS384: CreateHmacAlg(384),
    HS512: CreateHmacAlg(512),
    RS256: CreateHmacAlg(256),
    RS384: CreateHmacAlg(384),
    RS512: CreateHmacAlg(512)
};
function CreateHmacAlg(bits) {
    const sign = function sign(encoded, secret) {
        const sig = crypto.createHmac("sha" + bits, secret)
            .update(encoded)
            .digest("base64");
        return sig;
    };
    const verify = function verify(encoded, signature, secret) {
        const sig = sign(encoded, secret);
        return sig === signature;
    };
    return { sign, verify };
}
function CreateRsaAlg(bits) {
    const sign = function sign(encoded, secret) {
        const sig = crypto.createSign("SHA" + bits)
            .update(encoded)
            .sign(secret.toString(), "base64");
        return sig;
    };
    const verify = function verify(encoded, signature, secret) {
        const verifier = crypto.createVerify("RSA-SHA" + bits);
        verifier.update(encoded);
        return verifier.verify(secret, signature, "base64");
    };
    return { sign, verify };
}
/// UTILS ///
function JsonBase64Encode(obj) {
    const json = JSON.stringify(obj);
    return Base64ToUrlEncoded(Buffer.from(json).toString("base64"));
}
exports.JsonBase64Encode = JsonBase64Encode;
function JsonBase64Decode(str) {
    const decoded = Buffer.from(UrlEncodedToBase64(str), "base64").toString("utf-8");
    return JSON.parse(decoded);
}
exports.JsonBase64Decode = JsonBase64Decode;
function Base64ToUrlEncoded(base64) {
    return base64
        .replace(/=/g, "")
        .replace(/\+/g, "-")
        .replace(/\//g, "_");
}
exports.Base64ToUrlEncoded = Base64ToUrlEncoded;
function UrlEncodedToBase64(base64url) {
    base64url = base64url.toString();
    let padding = 4 - base64url.length % 4;
    if (padding !== 4) {
        for (let i = 0; i < padding; i++) {
            base64url += "=";
        }
    }
    return base64url
        .replace(/\-/g, "+")
        .replace(/_/g, "/");
}
exports.UrlEncodedToBase64 = UrlEncodedToBase64;
/// METHODS FOR ENCODING, DECODING, VERIFICATION ///
const DEFAULT_VERIFY_OPTIONS = {};
/**
 * Encode a token
 */
function Encode(payload, key, alg = "HS256") {
    if (!IsValidAlgorithm(alg)) {
        throw new Error(`Invalid algorithm. Got ${alg}. Must be one of ${exports.ALGORITHMS}`);
    }
    const header_b64 = JsonBase64Encode({ alg, typ: "JWT" });
    const payload_b64 = JsonBase64Encode(payload);
    const unsigned = `${header_b64}.${payload_b64}`;
    const signer = exports.Algorithms[alg];
    const sig = Base64ToUrlEncoded(signer.sign(unsigned, key));
    return `${unsigned}.${sig}`;
}
exports.Encode = Encode;
/**
 * Decode a token
 */
function Decode(token) {
    const parts = token.split(".");
    if (parts.length !== 3) {
        throw new Error(`Invalid token. Must have 3 parts. Got ${parts.length}`);
    }
    const header = JsonBase64Decode(parts[0]);
    const payload = JsonBase64Decode(parts[1]);
    const signature = Buffer.from(UrlEncodedToBase64(parts[2]), "base64");
    return { header, payload, signature };
}
exports.Decode = Decode;
/**
 * Verify a token
 */
function Verify(token, key, opts = DEFAULT_VERIFY_OPTIONS) {
    const decoded = Decode(token);
    const payload = decoded.payload;
    const parts = token.split(".");
    const alg = opts.alg || decoded.header.alg;
    const now = Date.now();
    const verifier = exports.Algorithms[alg];
    const result = {};
    if (opts.sig === undefined || opts.sig === true) {
        result.sig = verifier.verify(`${parts[0]}.${parts[1]}`, UrlEncodedToBase64(parts[2]), key);
    }
    if (opts.exp === true && payload.exp !== undefined) {
        result.exp = payload.exp < now;
    }
    if (opts.nbf === true && payload.nbf !== undefined) {
        result.nbf = payload.nbf <= now;
    }
    if (opts.iat !== undefined) {
        result.iat = payload.iat === opts.iat;
    }
    if (opts.iss !== undefined) {
        result.iss = payload.iss === opts.iss;
    }
    if (opts.jti !== undefined) {
        result.jti = payload.jti !== opts.jti;
    }
    if (opts.sub !== undefined) {
        result.sub = payload.sub === opts.sub;
    }
    if (opts.aud !== undefined) {
        result.aud = payload.aud === opts.aud;
    }
    return result;
}
exports.Verify = Verify;
// export { JwtHeader, JwtPayload, JwtToken, VerifyOptions, VerifyResult, IsVerifyValid };
//# sourceMappingURL=jwt.js.map