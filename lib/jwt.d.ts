/// <reference types="node" />
export interface JwtPayload {
    sub?: string | number;
    iat?: number;
    nbf?: number;
    exp?: number;
    jti?: string;
    iss?: string;
    [k: string]: any;
}
export interface JwtHeader {
    alg: string;
    typ: "JWT";
}
export interface JwtToken {
    header: JwtHeader;
    payload: JwtPayload;
    signature: Buffer;
}
export interface VerifyOptions {
    sig?: boolean;
    alg?: string;
    iat?: number;
    nbf?: boolean;
    exp?: boolean;
    jti?: string;
    iss?: string;
    sub?: string | number;
    aud?: string | number;
}
export interface VerifyResult {
    sig?: boolean;
    iat?: boolean;
    nbf?: boolean;
    exp?: boolean;
    jti?: boolean;
    iss?: boolean;
    sub?: boolean;
    aud?: boolean;
}
export declare function IsVerifyValid(opts: VerifyOptions, result: VerifyResult): boolean;
export declare const ALGORITHMS: string[];
export declare function IsValidAlgorithm(alg: string): boolean;
export interface IAlgorithm {
    sign(encoded: string, secret: string | Buffer): string;
    verify(encoded: string, signature: string, secret: string | Buffer): boolean;
}
export declare const Algorithms: {
    [k: string]: IAlgorithm;
};
export declare function JsonBase64Encode(obj: any): string;
export declare function JsonBase64Decode(str: string): any;
export declare function Base64ToUrlEncoded(base64: string): string;
export declare function UrlEncodedToBase64(base64url: string): string;
/**
 * Encode a token
 */
export declare function Encode(payload: JwtPayload, key: string | Buffer, alg?: string): string;
/**
 * Decode a token
 */
export declare function Decode(token: string): JwtToken;
/**
 * Verify a token
 */
export declare function Verify(token: string, key: string | Buffer, opts?: VerifyOptions): VerifyResult;
