/// <reference types="express-serve-static-core" />
import { PrismResponse } from "./";
import { AxiosRequestConfig } from "axios";
export declare type ExpressErrorRequestHandler = (err: any, req: Express.Request, res: Express.Response, next: Function) => void;
export declare let errorHandlers: ExpressErrorRequestHandler[];
export declare let defaultErrorLogger: (toLog: any) => void;
export declare let defaultWarnLogger: (toLog: any) => void;
export declare let defaultInfoLogger: (toLog: any) => void;
export declare function UrlJoin(...parts: string[]): string;
export declare function WrappedGet<T>(url: string, config?: AxiosRequestConfig): Promise<[any, PrismResponse<T> | undefined]>;
export declare function WrappedPost<T>(url: string, postdata?: any, config?: AxiosRequestConfig): Promise<[any, PrismResponse<T> | undefined]>;
