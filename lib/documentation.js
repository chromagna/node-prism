"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDocsAsHTML = exports.getDocs = void 0;
const controller_1 = require("./controller");
const U = require("./utils");
class ParamsDoc {
}
class ResponsesDoc {
}
class MethodDoc {
}
class ControllerDocNode {
}
function getControllerAndChildren(rootNodes, node) {
    let cdn = new ControllerDocNode();
    cdn.name = node.controller.ctr.name;
    cdn.docString = node.controller.docString;
    cdn.path = node.fullPath;
    cdn.middleware = node.controller.middleware;
    cdn.parent = undefined;
    cdn.children = [];
    cdn.methods = [];
    for (let methodKey in node.controller.methods) {
        let m = new MethodDoc();
        let method = node.controller.methods[methodKey];
        m.name = methodKey;
        m.docString = method.docString;
        m.mountpoints = method.methodMountpoints;
        m.middleware = method.middleware;
        m.params = [];
        m.responses = {};
        for (let rk in method.openApiResponses) {
            m.responses[rk] = method.openApiResponses[rk];
        }
        for (let param of method.extraParametersMappings) {
            if (param !== undefined) {
                m.params.push({
                    name: param.valueKey,
                    rvc: param.rvc,
                    valueKey: param.valueKey,
                    openApiType: param.openApiType,
                });
            }
        }
        cdn.methods.push(m);
    }
    for (let child of node.children) {
        let childCdn = getControllerAndChildren(rootNodes, child);
        childCdn.parent = cdn;
        cdn.children.push(childCdn);
    }
    rootNodes.push(cdn);
    return cdn;
}
function getDocs() {
    let cdns = [];
    for (let node of controller_1.globalCtrState.controllersTree) {
        getControllerAndChildren(cdns, node);
    }
    return cdns;
}
exports.getDocs = getDocs;
function s(num) {
    return ` `.repeat(num);
}
function getDocsAsHTML() {
    let cdns = getDocs();
    let content = `<html><head><style>${controller_1.docCss}</style></head><body style='background:#fff;'><pre>`;
    for (let cdn of cdns) {
        content += `<div class="docbox">`;
        content += `<div style="color:black;font-weight:500;">${cdn.name}</div>`;
        content += `<div style="font-family:Arial;color:#777777;font-weight:400;font-size:14px;">${cdn.docString}</div>`;
        content += `<div class="path">${cdn.path}</div>`;
        if (cdn.middleware.length > 0) {
            content += `<div style="margin-bottom: 10px;">`;
            for (let mw of cdn.middleware) {
                content += `<div style="color:#b06a37;">${mw.name || "Anonymous middleware function"}</div>`;
            }
            content += `</div>`;
        }
        if (cdn.parent !== undefined) {
            content += `<div style="color:orange;">${s(4)}[parent] ${cdn.parent.name}</div>`;
        }
        if (cdn.children.length > 0) {
            content += `<div style="color:orange;">${s(4)}[children]</div>`;
            for (let child of cdn.children) {
                content += `<div>${s(12)}${child.name}</div>`;
            }
        }
        if (cdn.methods.length > 0) {
            content += `<div class="mountPointWrapper">`;
            for (let method of cdn.methods) {
                content += `<div style="color:black;margin-top:5px;padding-top:5px;text-indent:-11.5em;padding-left:11.5em;">${s(12)}${method.name}() <span style="color:#777;white-space:pre-wrap;">${method.docString}</span></div>`;
                for (let mp of method.mountpoints) {
                    content += `<div style="margin-top:0px;">`;
                    content += `<span style="color:#777;width:40px;">${s(19)}${mp.httpMethod.toUpperCase()}</span>`;
                    // PATCH = 2
                    // POST = 3
                    // GET = 4
                    content += s(7 - mp.httpMethod.toUpperCase().length);
                    // content += `<span style="color:#777777;">@</span>`;
                    content += `<span style="color:blueviolet;">`;
                    if (mp.httpMethod.toUpperCase() === "GET") {
                        content += `<a href="${U.UrlJoin(cdn.path, "/", mp.path)}">`;
                    }
                    content += `${U.UrlJoin(cdn.path, "/", mp.path)}`;
                    if (mp.httpMethod.toUpperCase() === "GET") {
                        content += `</a>`;
                    }
                    content += `</span></div>`;
                }
                for (let epm of method.params) {
                    content += `<div style="color:#777777;">${s(26)}Receives from <span style="font-weight:bold;">${epm.rvc}</span>:`;
                    if (epm.openApiType !== undefined) {
                        content += ` property: <span style="color:blueviolet;">${epm.openApiType}</span>`;
                    }
                    if (epm.valueKey !== undefined) {
                        content += ` key: <span style="color:blueviolet;">${epm.valueKey}</span>`;
                    }
                    if (epm.rvc === "path") {
                        content += ` <span style="color:#999;">(ie: req.params.${epm.valueKey})</span>`;
                    }
                    if (epm.rvc === "query") {
                        content += ` <span style="color:#999;">(ie: req.query.${epm.valueKey})</span>`;
                    }
                    if (epm.rvc === "body") {
                        content += ` <span style="color:#999;">(ie: req.body.${epm.valueKey})</span>`;
                    }
                    content += `</div>`;
                }
                for (let emw of method.middleware) {
                    let name = emw.name;
                    content += `<div style="color:#b06a37;">${s(26)}${name || "Anonymous middleware function"}</div>`;
                }
            }
            content += `</div>`;
        }
        content += `</div>`; // close surrounding div
    }
    content += "</pre></body></html>";
    return content;
}
exports.getDocsAsHTML = getDocsAsHTML;
//# sourceMappingURL=documentation.js.map