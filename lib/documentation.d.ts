import * as Express from "express";
import { PrismMethodMountpoint } from "./controller";
declare class ParamsDoc {
    name: string;
    rvc: string;
    valueKey: string;
    openApiType: string;
}
declare class ResponsesDoc {
    [httpMethod: string]: {
        description: string;
        type: string;
    };
}
declare class MethodDoc {
    name: string;
    docString: string;
    mountpoints: PrismMethodMountpoint[];
    middleware: Express.RequestHandler[];
    params: ParamsDoc[];
    responses: ResponsesDoc;
}
declare class ControllerDocNode {
    name: string;
    docString: string;
    path: string;
    middleware: Express.RequestHandler[];
    parent: ControllerDocNode;
    children: ControllerDocNode[];
    methods: MethodDoc[];
}
export declare function getDocs(): ControllerDocNode[];
export declare function getDocsAsHTML(): string;
export {};
