"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isEmail = void 0;
// https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
function isEmail(str) {
    return emailRegex.test(str);
}
exports.isEmail = isEmail;
//# sourceMappingURL=email.js.map