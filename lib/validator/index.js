"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecordHandler = exports.OptionalHandler = exports.NullHandler = exports.Handler = exports.UnionHandler = exports.AnyHandler = exports.ArrayHandler = exports.BooleanHandler = exports.NumberHandler = exports.ObjectHandler = exports.StringHandler = exports.createExecutableSchema = exports.createSchema = void 0;
const handlers_1 = require("./handlers");
Object.defineProperty(exports, "Handler", { enumerable: true, get: function () { return handlers_1.Handler; } });
const any_1 = require("./handlers/any");
Object.defineProperty(exports, "AnyHandler", { enumerable: true, get: function () { return any_1.AnyHandler; } });
const array_1 = require("./handlers/array");
Object.defineProperty(exports, "ArrayHandler", { enumerable: true, get: function () { return array_1.ArrayHandler; } });
const boolean_1 = require("./handlers/boolean");
Object.defineProperty(exports, "BooleanHandler", { enumerable: true, get: function () { return boolean_1.BooleanHandler; } });
const null_1 = require("./handlers/null");
Object.defineProperty(exports, "NullHandler", { enumerable: true, get: function () { return null_1.NullHandler; } });
const number_1 = require("./handlers/number");
Object.defineProperty(exports, "NumberHandler", { enumerable: true, get: function () { return number_1.NumberHandler; } });
const object_1 = require("./handlers/object");
Object.defineProperty(exports, "ObjectHandler", { enumerable: true, get: function () { return object_1.ObjectHandler; } });
const optional_1 = require("./handlers/optional");
Object.defineProperty(exports, "OptionalHandler", { enumerable: true, get: function () { return optional_1.OptionalHandler; } });
const record_1 = require("./handlers/record");
Object.defineProperty(exports, "RecordHandler", { enumerable: true, get: function () { return record_1.RecordHandler; } });
const string_1 = require("./handlers/string");
Object.defineProperty(exports, "StringHandler", { enumerable: true, get: function () { return string_1.StringHandler; } });
const union_1 = require("./handlers/union");
Object.defineProperty(exports, "UnionHandler", { enumerable: true, get: function () { return union_1.UnionHandler; } });
exports.default = {
    object: (schema) => new object_1.ObjectHandler(schema),
    record: (schema) => new record_1.RecordHandler(schema),
    string: () => new string_1.StringHandler(),
    number: () => new number_1.NumberHandler(),
    boolean: () => new boolean_1.BooleanHandler(),
    array: (handler) => new array_1.ArrayHandler(handler),
    any: () => new any_1.AnyHandler(),
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    union: (handlers) => new union_1.UnionHandler(handlers),
    null: () => new null_1.NullHandler(),
    optional: () => new optional_1.OptionalHandler(),
    undefined: () => new optional_1.OptionalHandler(),
};
var schema_1 = require("./schema");
Object.defineProperty(exports, "createSchema", { enumerable: true, get: function () { return schema_1.createSchema; } });
Object.defineProperty(exports, "createExecutableSchema", { enumerable: true, get: function () { return schema_1.createExecutableSchema; } });
//# sourceMappingURL=index.js.map