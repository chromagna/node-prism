"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.is = exports.createSchema = exports.createExecutableSchema = void 0;
const object_1 = require("./handlers/object");
function getResultMessage(result) {
    let s = "";
    for (const err in result) {
        for (const k of Object.keys(err)) {
            s += `"${(k)}": ${k.message}. `;
        }
    }
    return s.trim();
}
function createExecutableSchema(handler) {
    return (value) => {
        const result = handler.validate(value, [], value);
        return {
            ok: !result.length,
            errors: result,
            message: getResultMessage(result),
        };
    };
}
exports.createExecutableSchema = createExecutableSchema;
function createSchema(def) {
    return createExecutableSchema(new object_1.ObjectHandler(def));
}
exports.createSchema = createSchema;
function is(value, handler) {
    return createExecutableSchema(handler)(value).ok;
}
exports.is = is;
//# sourceMappingURL=schema.js.map