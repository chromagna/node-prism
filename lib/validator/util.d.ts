export declare function isObject(val: unknown): val is Record<string, unknown>;
