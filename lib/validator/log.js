"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.log = void 0;
function log(...args) {
    if (process.env.DEVELOPMENT) {
        console.error(...args);
    }
}
exports.log = log;
//# sourceMappingURL=log.js.map