"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isObject = void 0;
function isObject(val) {
    return typeof val === "object" && !Array.isArray(val) && val !== null;
}
exports.isObject = isObject;
//# sourceMappingURL=util.js.map