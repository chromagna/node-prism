"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ObjectHandler = void 0;
const union_1 = require("./union");
const optional_1 = require("./optional");
const util_1 = require("../util");
const base_1 = require("./base");
class ObjectHandler extends base_1.BaseHandler {
    constructor(keys) {
        super();
        this._keys = {};
        this._arbitrary = false;
        this._partial = false;
        this._rules.push((v) => util_1.isObject(v) || "Must be an object");
        if (keys) {
            this._keys = keys;
        }
    }
    any(pred) {
        return this.some(pred);
    }
    all(pred) {
        return this.every(pred);
    }
    some(pred) {
        this._rules.push((o) => Object.keys(o).some((k) => pred(o[k], k, o)));
        return this;
    }
    every(pred) {
        this._rules.push((o) => Object.keys(o).every((k) => pred(o[k], k, o)));
        return this;
    }
    partial() {
        this._partial = true;
        return this;
    }
    arbitrary() {
        this._arbitrary = true;
        return this;
    }
    numKeys(num) {
        this._rules.push((v) => Object.keys(v).length === num || `Must have ${num} keys`);
        return this;
    }
    validate(value, key = [], root) {
        let myResults = [];
        const keyResults = [];
        if (typeof value === "object" && value !== null) {
            const _value = value;
            if (!this._arbitrary) {
                for (const objKey in _value) {
                    const handler = this._keys[objKey];
                    if (!handler) {
                        keyResults.push({
                            key: [...key, objKey],
                            message: "Value not allowed",
                        });
                    }
                }
            }
            for (const myKey in this._keys) {
                const handler = this._keys[myKey];
                const getResults = (handler) => {
                    const results = handler.validate(_value[myKey], [...key, myKey], root);
                    keyResults.push(...results);
                    return results;
                };
                if (this._partial) {
                    getResults(new union_1.UnionHandler([handler, new optional_1.OptionalHandler()]));
                }
                else {
                    getResults(handler);
                }
            }
        }
        if (!keyResults.length) {
            myResults = super.validate(value, key, root);
        }
        return myResults.concat(keyResults);
    }
}
exports.ObjectHandler = ObjectHandler;
//# sourceMappingURL=object.js.map