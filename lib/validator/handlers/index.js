"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Handler = void 0;
const log_1 = require("../log");
class Handler {
    constructor() {
        this._rules = [];
    }
    test(rule) {
        this.custom(rule);
        return this;
    }
    check(rule) {
        this.custom(rule);
        return this;
    }
    use(rule) {
        this.custom(rule);
        return this;
    }
    rule(rule) {
        this.custom(rule);
        return this;
    }
    custom(rule) {
        this._rules.push(rule);
        return this;
    }
    validate(value, key, root) {
        const results = [];
        log_1.log("Checking rules");
        for (const rule of this._rules) {
            const result = rule(value, key || [], root || value);
            if (typeof result === "string" || !result) {
                log_1.log("Rule failed!");
                results.push({ key: key || [], message: result });
                log_1.log("Rule result: " + result);
                return results;
            }
            else {
                log_1.log("Rule passed!");
            }
        }
        log_1.log(`Checked rules, ${results.length} errors`);
        return results;
    }
}
exports.Handler = Handler;
//# sourceMappingURL=index.js.map