import { IValidationResult, Rule } from "../types";
export declare abstract class Handler<T = any> {
    _type: any;
    protected _rules: Rule<T>[];
    test(rule: Rule<T>): this;
    check(rule: Rule<T>): this;
    use(rule: Rule<T>): this;
    rule(rule: Rule<T>): this;
    custom(rule: Rule<T>): this;
    validate(value: unknown, key?: string[], root?: unknown): IValidationResult[];
}
