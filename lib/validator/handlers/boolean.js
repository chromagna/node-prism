"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BooleanHandler = void 0;
const atomic_1 = require("./atomic");
/**
 * Boolean handler
 */
class BooleanHandler extends atomic_1.AtomicHandler {
    constructor() {
        super();
        this._rules.push((v) => typeof v === "boolean" || "Must be a boolean");
    }
    /**
     * Only allows true
     */
    true() {
        return this.equals(true);
    }
    /**
     * Only allows false
     */
    false() {
        return this.equals(false);
    }
}
exports.BooleanHandler = BooleanHandler;
//# sourceMappingURL=boolean.js.map