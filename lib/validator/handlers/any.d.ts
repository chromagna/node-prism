import { AtomicHandler } from "./atomic";
/**
 * Validate any value, use rules to narrow it down
 */
export declare class AnyHandler extends AtomicHandler<any> {
    constructor();
    /**
     * Only allow truthy values (https://developer.mozilla.org/en-US/docs/Glossary/Truthy)
     */
    truthy(): this;
    /**
     * Only allow falsy values(https://developer.mozilla.org/en-US/docs/Glossary/Falsy)
     */
    falsy(): this;
}
