import { AtomicHandler } from "./atomic";
/**
 * Boolean handler
 */
export declare class BooleanHandler extends AtomicHandler<boolean> {
    constructor();
    /**
     * Only allows true
     */
    true(): this;
    /**
     * Only allows false
     */
    false(): this;
}
