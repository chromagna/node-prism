import { AtomicHandler } from "./atomic";
export declare class NumberHandler extends AtomicHandler<number> {
    constructor();
    natural(opts?: Partial<{
        withZero: boolean;
    }>): this;
    int(): this;
    integer(): this;
    negative(opts?: Partial<{
        withZero: boolean;
    }>): this;
    positive(opts?: Partial<{
        withZero: boolean;
    }>): this;
    between(min: number, max: number): this;
    min(min: number): this;
    max(max: number): this;
}
