import { Handler } from "./index";
import { NullHandler } from "./null";
import { OptionalHandler } from "./optional";
export declare class UnionHandler<T extends [Handler, Handler, ...Handler[]]> extends Handler {
    _type: T[number]["_type"];
    /**
     * Allows null value
     */
    nullable(): UnionHandler<[this, NullHandler]>;
    /**
     * Allows undefined value
     */
    optional(): UnionHandler<[this, OptionalHandler]>;
    constructor(handlers: T);
}
