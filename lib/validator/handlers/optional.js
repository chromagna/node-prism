"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UndefinedHandler = exports.OptionalHandler = void 0;
const index_1 = require("./index");
const union_1 = require("./union");
const null_1 = require("./null");
class OptionalHandler extends index_1.Handler {
    constructor() {
        super();
        this._rules.push((v) => v === undefined || "Must be undefined");
    }
    /**
     * Allows null value
     */
    nullable() {
        return new union_1.UnionHandler([this, new null_1.NullHandler()]);
    }
}
exports.OptionalHandler = OptionalHandler;
exports.UndefinedHandler = OptionalHandler;
//# sourceMappingURL=optional.js.map