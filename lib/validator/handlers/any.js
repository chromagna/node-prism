"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnyHandler = void 0;
const atomic_1 = require("./atomic");
/**
 * Validate any value, use rules to narrow it down
 */
class AnyHandler extends atomic_1.AtomicHandler {
    constructor() {
        super();
    }
    /**
     * Only allow truthy values (https://developer.mozilla.org/en-US/docs/Glossary/Truthy)
     */
    truthy() {
        this._rules.push((v) => !!v || "Value must be truthy");
        return this;
    }
    /**
     * Only allow falsy values(https://developer.mozilla.org/en-US/docs/Glossary/Falsy)
     */
    falsy() {
        this._rules.push((v) => !v || "Value must be falsy");
        return this;
    }
}
exports.AnyHandler = AnyHandler;
//# sourceMappingURL=any.js.map