"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StringHandler = void 0;
const atomic_1 = require("./atomic");
const email_1 = require("../email");
class StringHandler extends atomic_1.AtomicHandler {
    constructor() {
        super();
        this._rules.push((v) => typeof v === "string" || "Must be a string");
    }
    prefix(substr) {
        return this.startsWith(substr);
    }
    suffix(substr) {
        return this.endsWith(substr);
    }
    endsWith(substr) {
        this._rules.push((v) => v.endsWith(substr) || `Value has to end with ${substr}`);
        return this;
    }
    beginsWith(substr) {
        return this.startsWith(substr);
    }
    startsWith(substr) {
        this._rules.push((v) => v.startsWith(substr) || `Value has to start with ${substr}`);
        return this;
    }
    email() {
        this._rules.push((v) => email_1.isEmail(v) || `Must be a valid email`);
        return this;
    }
    numeric() {
        const regexp = /[^0-9]/;
        this._rules.push((v) => (v.length && !regexp.test(v)) || `Must be numeric`);
        return this;
    }
    alphanum(allowSpaces) {
        const regexp = allowSpaces ? /[^a-zA-Z0-9 ]/ : /[^a-zA-Z0-9]/;
        this._rules.push((v) => (v.length && !regexp.test(v)) || `Must be alphanumeric`);
        return this;
    }
    regex(regexp) {
        this._rules.push((v) => regexp.test(v) || `Does not match ${regexp.toString()}`);
        return this;
    }
    match(regexp) {
        return this.regex(regexp);
    }
    pattern(regexp) {
        return this.regex(regexp);
    }
    length(num) {
        this._rules.push((v) => v.length === num || `Must be of length ${num}`);
        return this;
    }
    len(num) {
        return this.length(num);
    }
    notEmpty( /* TODO: trim? */) {
        return this.min(1);
    }
    between(min, max) {
        return this.min(min).max(max);
    }
    min(min) {
        this._rules.push((v) => v.length >= min || `Must be at least ${min} characters long`);
        return this;
    }
    max(max) {
        this._rules.push((v) => v.length <= max || `Must have at most ${max} characters`);
        return this;
    }
}
exports.StringHandler = StringHandler;
//# sourceMappingURL=string.js.map