import { AtomicHandler } from "./atomic";
export declare class StringHandler extends AtomicHandler<string> {
    constructor();
    prefix(substr: string): this;
    suffix(substr: string): this;
    endsWith(substr: string): this;
    beginsWith(substr: string): this;
    startsWith(substr: string): this;
    email(): this;
    numeric(): this;
    alphanum(allowSpaces?: boolean): this;
    regex(regexp: RegExp): this;
    match(regexp: RegExp): this;
    pattern(regexp: RegExp): this;
    length(num: number): this;
    len(num: number): this;
    notEmpty(): this;
    between(min: number, max: number): this;
    min(min: number): this;
    max(max: number): this;
}
