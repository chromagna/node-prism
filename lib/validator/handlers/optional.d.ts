import { Handler } from "./index";
import { UnionHandler } from "./union";
import { NullHandler } from "./null";
export declare class OptionalHandler extends Handler {
    _type: undefined;
    constructor();
    /**
     * Allows null value
     */
    nullable(): UnionHandler<[this, NullHandler]>;
}
export declare const UndefinedHandler: typeof OptionalHandler;
