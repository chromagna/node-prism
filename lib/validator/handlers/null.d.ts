import { Handler } from "./index";
import { UnionHandler } from "./union";
import { OptionalHandler } from "./optional";
/**
 * Null handler
 */
export declare class NullHandler extends Handler {
    _type: null;
    constructor();
    /**
     * Allows undefined value
     */
    optional(): UnionHandler<[this, OptionalHandler]>;
}
