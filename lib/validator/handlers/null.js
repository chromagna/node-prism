"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NullHandler = void 0;
const index_1 = require("./index");
const union_1 = require("./union");
const optional_1 = require("./optional");
/**
 * Null handler
 */
class NullHandler extends index_1.Handler {
    constructor() {
        super();
        this._rules.push((v) => v === null || "Must be null");
    }
    /**
     * Allows undefined value
     */
    optional() {
        return new union_1.UnionHandler([this, new optional_1.OptionalHandler()]);
    }
}
exports.NullHandler = NullHandler;
//# sourceMappingURL=null.js.map