"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseHandler = void 0;
const index_1 = require("./index");
const union_1 = require("./union");
const null_1 = require("./null");
const optional_1 = require("./optional");
/**
 * Base handler
 */
class BaseHandler extends index_1.Handler {
    /**
     * Allows null value
     */
    nullable() {
        return new union_1.UnionHandler([this, new null_1.NullHandler()]);
    }
    /**
     * Allows undefined value
     */
    optional() {
        return new union_1.UnionHandler([this, new optional_1.OptionalHandler()]);
    }
}
exports.BaseHandler = BaseHandler;
//# sourceMappingURL=base.js.map