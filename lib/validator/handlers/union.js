"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnionHandler = void 0;
const index_1 = require("./index");
const null_1 = require("./null");
const optional_1 = require("./optional");
class UnionHandler extends index_1.Handler {
    constructor(handlers) {
        super();
        this._rules.push((v) => {
            if (handlers.some((h) => h.validate(v).length === 0)) {
                return true;
            }
            // TODO: collect errors and display?
            return "Input is not matching any of the expected schemas";
        });
    }
    /**
     * Allows null value
     */
    nullable() {
        return new UnionHandler([this, new null_1.NullHandler()]);
    }
    /**
     * Allows undefined value
     */
    optional() {
        return new UnionHandler([this, new optional_1.OptionalHandler()]);
    }
}
exports.UnionHandler = UnionHandler;
//# sourceMappingURL=union.js.map