import { Handler } from "./index";
import { IValidationResult } from "../types";
import { Infer } from "../index";
import { BaseHandler } from "./base";
export declare class RecordHandler<T extends Handler> extends BaseHandler {
    _type: Record<string, Infer<T>>;
    private _schema;
    constructor(schema: T);
    any(pred: (v: Infer<T>, k: string, obj: any) => boolean): this;
    all(pred: (v: Infer<T>, k: string, obj: any) => boolean): this;
    some(pred: (v: Infer<T>, k: string, obj: any) => boolean): this;
    every(pred: (v: Infer<T>, k: string, obj: any) => boolean): this;
    numKeys(num: number): this;
    validate(value: unknown, key?: string[], root?: unknown): IValidationResult[];
}
