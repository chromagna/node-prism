"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AtomicHandler = void 0;
const base_1 = require("./base");
/**
 * Base handler for atomic values
 */
class AtomicHandler extends base_1.BaseHandler {
    equals(expected) {
        this._rules.push((v) => v === expected || `Must be equal to ${expected}`);
        return this;
    }
    eq(expected) {
        return this.equals(expected);
    }
    equal(expected) {
        return this.equals(expected);
    }
    enum(values) {
        this._rules.push((v) => values.includes(v) ||
            `Must be one of the following values: ${values.join(", ")}`);
        return this;
    }
}
exports.AtomicHandler = AtomicHandler;
//# sourceMappingURL=atomic.js.map