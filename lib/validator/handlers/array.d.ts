import { Handler } from "./index";
import { IValidationResult } from "../types";
import { Infer } from "../index";
import { BaseHandler } from "./base";
export declare class ArrayHandler<T extends Handler> extends BaseHandler {
    _type: Array<Infer<T>>;
    _handler: Handler;
    constructor(handler: T);
    any(pred: (v: Infer<T>, i: number, arr: Array<Infer<T>>) => boolean): this;
    all(pred: (v: Infer<T>, i: number, arr: Array<Infer<T>>) => boolean): this;
    some(pred: (v: Infer<T>, i: number, arr: Array<Infer<T>>) => boolean): this;
    every(pred: (v: Infer<T>, i: number, arr: Array<Infer<T>>) => boolean): this;
    length(num: number): this;
    len(num: number): this;
    notEmpty(): this;
    between(min: number, max: number): this;
    min(min: number): this;
    max(max: number): this;
    validate(value: unknown, key?: string[], root?: unknown): IValidationResult[];
}
