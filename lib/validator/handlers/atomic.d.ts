import { BaseHandler } from "./base";
/**
 * Base handler for atomic values
 */
export declare abstract class AtomicHandler<T = string | number | boolean> extends BaseHandler<T> {
    _type: T;
    equals(expected: T): this;
    eq(expected: T): this;
    equal(expected: T): this;
    enum(values: T[]): this;
}
