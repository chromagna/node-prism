import { Handler } from "./index";
import { IValidationResult, Infer } from "../types";
import { BaseHandler } from "./base";
export declare class ObjectHandler<T extends Record<string, Handler>> extends BaseHandler {
    _type: {
        [K in keyof T]: Infer<T[K]>;
    };
    private _keys;
    private _arbitrary;
    private _partial;
    constructor(keys?: T);
    any(pred: (v: unknown, k: string, obj: {
        [K in keyof T]: Infer<T[K]>;
    }) => boolean): this;
    all(pred: (v: unknown, k: string, obj: {
        [K in keyof T]: Infer<T[K]>;
    }) => boolean): this;
    some(pred: (v: unknown, k: string, obj: {
        [K in keyof T]: Infer<T[K]>;
    }) => boolean): this;
    every(pred: (v: unknown, k: string, obj: {
        [K in keyof T]: Infer<T[K]>;
    }) => boolean): this;
    partial(): this;
    arbitrary(): this;
    numKeys(num: number): this;
    validate(value: unknown, key?: string[], root?: unknown): IValidationResult[];
}
