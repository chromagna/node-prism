"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NumberHandler = void 0;
const atomic_1 = require("./atomic");
class NumberHandler extends atomic_1.AtomicHandler {
    constructor() {
        super();
        this._rules.push((v) => (typeof v === "number" && !isNaN(v)) || "Must be a number");
    }
    natural(opts) {
        return this.integer().positive({ withZero: opts === null || opts === void 0 ? void 0 : opts.withZero });
    }
    int() {
        return this.integer();
    }
    integer() {
        this._rules.push((v) => Number.isInteger(v) || `Must be an integer`);
        return this;
    }
    negative(opts) {
        if (opts === null || opts === void 0 ? void 0 : opts.withZero) {
            this.max(0);
        }
        else {
            this._rules.push((v) => v < 0 || `Must be a negative number`);
        }
        return this;
    }
    positive(opts) {
        if (opts === null || opts === void 0 ? void 0 : opts.withZero) {
            this.min(0);
        }
        else {
            this._rules.push((v) => v > 0 || `Must be a positive number`);
        }
        return this;
    }
    between(min, max) {
        return this.min(min).max(max);
    }
    min(min) {
        this._rules.push((v) => v >= min || `Must be ${min} or greater`);
        return this;
    }
    max(max) {
        this._rules.push((v) => v <= max || `Must be less than or equal ${max}`);
        return this;
    }
}
exports.NumberHandler = NumberHandler;
//# sourceMappingURL=number.js.map