"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecordHandler = void 0;
const util_1 = require("../util");
const base_1 = require("./base");
class RecordHandler extends base_1.BaseHandler {
    constructor(schema) {
        super();
        this._rules.push((v) => util_1.isObject(v) || "Must be an object");
        this._schema = schema;
    }
    any(pred) {
        return this.some(pred);
    }
    all(pred) {
        return this.every(pred);
    }
    some(pred) {
        this._rules.push((o) => Object.keys(o).some((k) => pred(o[k], k, o)));
        return this;
    }
    every(pred) {
        this._rules.push((o) => Object.keys(o).every((k) => pred(o[k], k, o)));
        return this;
    }
    numKeys(num) {
        this._rules.push((v) => Object.keys(v).length === num || `Must have ${num} keys`);
        return this;
    }
    validate(value, key = [], root) {
        let myResults = [];
        const keyResults = [];
        if (typeof value === "object" && value !== null) {
            const _value = value;
            for (const myKey in _value) {
                const results = this._schema.validate(_value[myKey], [...key, myKey], root);
                keyResults.push(...results);
            }
        }
        if (!keyResults.length) {
            myResults = super.validate(value, key, root);
        }
        return myResults.concat(keyResults);
    }
}
exports.RecordHandler = RecordHandler;
//# sourceMappingURL=record.js.map