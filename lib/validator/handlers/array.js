"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrayHandler = void 0;
const base_1 = require("./base");
const arrayRule = (v) => Array.isArray(v) || "Must be an array";
class ArrayHandler extends base_1.BaseHandler {
    constructor(handler) {
        super();
        this._rules.push(arrayRule);
        this._handler = handler;
    }
    any(pred) {
        return this.some(pred);
    }
    all(pred) {
        return this.every(pred);
    }
    some(pred) {
        this._rules.push((arr) => arr.some(pred));
        return this;
    }
    every(pred) {
        this._rules.push((arr) => arr.every(pred));
        return this;
    }
    length(num) {
        this._rules.push((v) => v.length === num || `Must be of length ${num}`);
        return this;
    }
    len(num) {
        this.length(num);
        return this;
    }
    notEmpty() {
        this._rules.push((v) => !!v.length || `Must not be empty`);
        return this;
    }
    between(min, max) {
        return this.min(min).max(max);
    }
    min(min) {
        this._rules.push((v) => v.length >= min || `Must have at least ${min} items`);
        return this;
    }
    max(max) {
        this._rules.push((v) => v.length <= max || `Must have at most ${max} items`);
        return this;
    }
    validate(value, key = [], root) {
        let myResults = [];
        const keyResults = [];
        if (Array.isArray(value)) {
            value.forEach((v, i) => {
                const myKey = i.toString();
                const results = this._handler.validate(v, [...key, myKey], root);
                keyResults.push(...results);
            });
        }
        if (!keyResults.length) {
            myResults = super.validate(value, key, root);
        }
        return myResults.concat(keyResults);
    }
}
exports.ArrayHandler = ArrayHandler;
//# sourceMappingURL=array.js.map