import { Handler } from "./handlers";
import { Infer, ISchemaDefinition } from "./types";
export declare function createExecutableSchema(handler: Handler): (value: unknown) => {
    ok: boolean;
    errors: import("./types").IValidationResult[];
    message: string;
};
export declare function createSchema(def: ISchemaDefinition): (value: unknown) => {
    ok: boolean;
    errors: import("./types").IValidationResult[];
    message: string;
};
export declare function is<T extends Handler>(value: unknown, handler: T): value is Infer<T>;
