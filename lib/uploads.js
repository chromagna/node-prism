"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcessFileUploadRequest = void 0;
const B = require("busboy");
const FS = require("fs");
/**
 *
 * @param context Context
 * @param config
 * @returns Throws an Error on failure, Promise of FileRequest if it succeeds.
 */
function ProcessFileUploadRequest(context, config) {
    let request = context.request;
    let response = context.response;
    let b = new B({ "headers": request.headers });
    if (!config)
        config = {};
    config["dir"] = config["dir"] || "_uploads";
    config["scrambleFilename"] = config["scrambleFilename"] || false;
    config["formFieldName"] = config["formFieldName"] || "file";
    config["returnBuffer"] = config["returnBuffer"] || false;
    let servlet = new UploadServlet(config);
    return new Promise((resolve, reject) => {
        b.on("file", function (fieldname, file, filename, encoding, mimetype) {
            if (fieldname === config["formFieldName"]) {
                request["postFile"] = {
                    "filename": filename,
                    "data": null,
                };
                file.on("data", function (data) {
                    const oldData = request["postFile"]["data"];
                    const newData = oldData == null ? data : Buffer.concat([oldData, data]);
                    request["postFile"]["data"] = newData;
                });
            }
        });
        b.on("field", (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) => {
            if (fieldname === "data") {
                request.postData = val;
            }
        });
        b.on("finish", () => __awaiter(this, void 0, void 0, function* () {
            let success = yield servlet.doPost(request, response);
            if (success) {
                // Delete FileRequest.file Buffer? We don't need to return the file.
                if (servlet.req.file && !config["returnBuffer"])
                    delete servlet.req.file;
                return resolve(servlet.req);
            }
            throw new Error("Could not save file.");
        }));
        b.on("error", (e) => {
            throw new Error(e);
        });
        request.pipe(b);
    });
}
exports.ProcessFileUploadRequest = ProcessFileUploadRequest;
class UploadServlet {
    constructor(conf) {
        this.req = null;
        this.conf = conf;
    }
    getReq(request, response) {
        let dataJson = request.postData;
        let fileName = null;
        let fileSize = -1;
        let file;
        let orig = null;
        if (request["postFile"]) {
            file = request["postFile"]["data"];
            fileName = request["postFile"]["filename"];
            orig = fileName;
            fileSize = file.length;
        }
        if (this.getParam("scrambleFilename", false, false)) {
            let alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
            let id;
            let fp;
            let ext = fileName.split(".").pop();
            do {
                id = "";
                for (let i = 0; i < 6; i++) {
                    let charNumber = Math.floor(Math.random() * alphabet.length);
                    id += alphabet.substring(charNumber, charNumber + 1);
                }
                fp = this.getBaseDir() + id + "." + ext;
            } while (FS.existsSync(fp));
            fileName = id + "." + ext;
        }
        let req = {
            originalFileName: orig,
            fileName: fileName,
            fileSize: fileSize,
            file: file,
        };
        this.req = req;
        return req;
    }
    getParam(name, defaultValue, doAddTrailingSlash) {
        if (name in this.conf)
            return this.addTrailingSlash(this.conf[name], doAddTrailingSlash);
        else
            return defaultValue;
    }
    addTrailingSlash(value, doAddTrailingSlash) {
        if (value != null && doAddTrailingSlash && (value.length == 0 || value.substring(value.length - 1) !== "/"))
            value += "/";
        return value;
    }
    getBaseDir() {
        let d = __dirname;
        if (d.length === 1)
            d = d.substr(1); // "/" case
        let dir = this.getParam("dir", d + "files/", true);
        if (!FS.existsSync(dir)) {
            FS.mkdirSync(dir);
        }
        return dir;
    }
    getTmpDir() {
        let dir = this.getParam("tmp", this.getBaseDir() + "tmp/", true);
        if (!FS.existsSync(dir))
            FS.mkdirSync(dir);
        return dir;
    }
    doPost(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            let req = this.getReq(request, response);
            let file = new AFile(req, this.getBaseDir());
            file.upload();
            return true;
        });
    }
}
class AFile {
    constructor(req, baseDir) {
        this.req = req;
        this.baseDir = baseDir;
    }
    getFile() { return this.getFullPath(); }
    getFullPath() { return this.baseDir + this.req.fileName; }
    upload() {
        try {
            this.req.fullPath = this.getFullPath();
            FS.writeFileSync(this.getFullPath(), this.req.file);
        }
        catch (e) {
            throw new Error(`Failed to write file: ${e}`);
        }
    }
}
//# sourceMappingURL=uploads.js.map