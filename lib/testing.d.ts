import * as C from "./controller";
export declare let globalTestState: PrismTestsState;
/**
 * Registers a fixture of tests with the global test runner.
 * @param humanReadableName A human readable name.
 * @returns void
 */
export declare function Fixture(humanReadableName?: string): (f: Function) => void;
/**
 * Register a new test. If the test throws, it fails. Otherwise, it passes.
 * @param humanReadableName A human readable name.
 * @returns void
 */
export declare function Test(humanReadableName: string): (a: any, s: string, pd: PropertyDescriptor) => void;
/**
 * Method to run before any of the tests.
 * @returns void
 */
export declare function Before(): (a: any, s: string, pd: PropertyDescriptor) => void;
/**
 * Method to run after any of the tests.
 * @returns void
 */
export declare function After(): (a: any, s: string, pd: PropertyDescriptor) => void;
export declare type PrismFixtureMap = {
    [key: string]: PrismFixture;
};
export declare type PrismTestMap = {
    [key: string]: PrismTest;
};
export declare class PrismTestsState {
    fixtures: PrismFixtureMap;
    getOrInsertFixture(ctr: Function): PrismFixture;
    generateCompleteFixtureMetadata(): Object;
    static getFixtureHashId(fixture: PrismFixture): string;
    generateRainbowTables(): Object;
    run(testsToRun?: Object): Promise<PrismTestResult[]>;
}
export declare class PrismFixture {
    runBeforeMethods: string[];
    runAfterMethods: string[];
    tests: PrismTestMap;
    ctr: Function;
    humanReadableName: string;
    explicitlyDeclared: boolean;
    getOrInsertTest(key: string): PrismTest;
}
export declare class PrismTest {
    humanReadableName: string;
    explicitlyDeclared: boolean;
}
export declare class PrismTestResult {
    fixtureDesc: string;
    fixtureKey: string;
    testDesc: string;
    testKey: string;
    passed: boolean;
    message: string | object;
}
export declare function injectTestRunnerMiddleware(controller: C.PrismController): void;
