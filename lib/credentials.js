"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bearer = void 0;
class Bearer {
    static GetToken(req, options) {
        let proxy;
        if (options) {
            proxy = options.proxy || null;
        }
        const auth = req.headers
            ? proxy
                ? req.headers["proxy-authorization"]
                : req.headers.authorization
            : null;
        if (auth) {
            const [_, token] = auth.split(" ");
            return token;
        }
        else {
            return undefined;
        }
    }
}
exports.Bearer = Bearer;
//# sourceMappingURL=credentials.js.map