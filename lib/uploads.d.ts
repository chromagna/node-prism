/// <reference types="node" />
import { Context } from "./controller";
export interface IUploadConfig {
    /**
     * Directory to upload the file to. Relative path.
     */
    dir?: string;
    /**
     * Scramble the filename? If not, use the one passed in the request.
     */
    scrambleFilename?: boolean;
    /**
     * What's the name of the field in the FormData that includes the file?
     */
    formFieldName?: string;
    /**
     * If true, the file buffer will be returned inside the FileRequest object from ProcessFileUploadRequest.
     */
    returnBuffer?: boolean;
}
/**
 *
 * @param context Context
 * @param config
 * @returns Throws an Error on failure, Promise of FileRequest if it succeeds.
 */
export declare function ProcessFileUploadRequest(context: Context, config?: IUploadConfig): Promise<FileRequest>;
export interface FileRequest {
    originalFileName?: string;
    fileName?: string;
    fileSize?: number;
    file?: Buffer;
    fullPath?: string;
}
