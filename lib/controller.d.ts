/// <reference types="node" />
import * as Express from "express";
import * as Stream from "stream";
export declare const docCss = "\na{\n\tcolor:#775ad0;\n\ttext-decoration:none;\n}\na:hover{\n\tcolor:#b4b0c1;\n}\nhr{\n\tborder-top:2px solid #f4f5f7;\n\tborder-bottom:0px;\n\tborder-left:0px;\n\tborder-right:0px;\n}\n.mono{\n\tline-height:18px;\n\tfont-size:12px;\n\tfont-family:\"Fira Code\",\"Source Code Pro\",Menlo,Monaco,monospace,sans-serif,-apple-system,BlinkMacSystemFont;\n}\nbody{\n\tfont-family:Arial;\n\tfont-size:14px;\n\tpadding: 0;\n\tmargin: 0;\n}\npre{\n\tline-height:18px;\n\tfont-size:12px;\n\tfont-family:\"Fira Code\",\"Source Code Pro\",Menlo,Monaco,monospace,sans-serif,-apple-system,BlinkMacSystemFont;\n}\n.docbox{\n\tmargin-bottom:10px;\n\tpadding:40px;\n\tbackground:white;\n\tmargin-right:auto;\n\tmargin-left:auto;\n\tborder-bottom: 1px solid #bbb;\n}\n.mountPointWrapper{\n\tbackground: #f4f5f7;\n\tpadding: 5px;\n\tborder: 1px solid #c8c5c5;\n}\n.path {\n\tfont-size: 16px;\n\tcolor: blueviolet;\n\tmargin-bottom: 15px;\n\tfont-weight: 500;\n}\n";
/**
 * docbox
.docbox {
    margin-bottom:10px;
    padding:10px;
    background:white;
    box-shadow:0px 0px 1px 1px #00000038, 0px 4px 8px rgba(0, 0, 0, 0.15);
    border-radius:2px;
    width:700px;
    margin-right:auto;
    margin-left:auto;
}
 */
export interface IDisposable {
    dispose(): void;
}
export declare class Context {
    request: Express.Request;
    response: Express.Response;
    nextMiddleware: Function;
    private disposableInstance;
    constructor();
    create<T extends IDisposable>(tFactory: {
        new (...args: any[]): T;
    }, ...args: any[]): T;
    dispose(): void;
}
export declare class PrismResponse<T> {
    /**
     * An HTTP status code like 200, 401, 404, etc.
     *
     * @type {number}
     */
    code: number;
    /**
     * A message relevant to the result of this request.
     *
     * @type {string}
     */
    message?: string;
    /**
     * Whether or not a server-side error occurred. Always defined,
     * whether or not an HttpError or HttpSuccess<T> is returned, but
     * only true when the response is an HttpError.
     *
     * @type {boolean}
     */
    error?: boolean;
    /**
     * Whether or not the server considers your request a success. Always defined,
     * whether or not an HttpError or HttpSuccess<T> is returned, but only
     * true when the response is an HttpSuccess<T>.
     *
     * @type {boolean}
     */
    success?: boolean;
    /**
     * A placeholder for an internal reference identifier for this request intended
     * to be received from API consumers to help debug any particular request.
     *
     * @type {any}
     */
    reference?: any;
    /**
     * A container for the T generic in HttpSuccess<T> when the response is an
     * HttpSuccess<T>.
     *
     * @type {T}
     */
    data?: T;
    /**
     * If a Bearer token was present in the request, it will be returned.
     */
    token?: string | undefined;
    constructor(a?: HttpError, b?: HttpSuccess<T>);
}
declare class HttpError {
    success?: boolean | undefined;
    error?: boolean | undefined;
    code: number;
    message?: string;
    token?: string;
    constructor(context: Context, code: number, messageOrError: string | Error | Object);
}
declare class HttpSuccess<T> implements HttpError {
    code: number;
    success?: boolean | undefined;
    error?: boolean | undefined;
    token: string;
    data: any;
    message: string;
    reference: string | number;
    constructor(code: number, token: string, data: T, message: string, reference: number | string);
}
export declare enum HttpStatusCode {
    OK = 200,
    Created = 201,
    BadRequest = 400,
    Unauthorized = 401,
    Forbidden = 403,
    NotFound = 404,
    UnprocessableEntity = 422,
    InternalServerError = 500,
    TooManyRequests = 429
}
export interface IResponse {
    TooManyRequests(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    BadRequest(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    Unauthorized(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    NotFound(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    Forbidden(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    InternalServerError(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    Success<T>(context: Context, data: T, message: string, reference: number | string): PrismResponse<T>;
}
declare class ResponsesClass implements IResponse {
    TooManyRequests(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    BadRequest(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    Unauthorized(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    NotFound(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    Forbidden(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    InternalServerError(context: Context, messageOrError?: string | Object | Error): PrismResponse<any>;
    Success<T>(context: Context, data?: T, message?: string, reference?: number | string): PrismResponse<T>;
    OK<T>(context: Context, data?: T, message?: string, reference?: number | string): PrismResponse<T>;
}
export declare const Responses: ResponsesClass;
export declare class FileServe {
    file: string | Stream.Readable;
    filename: string;
    contentType: string;
    forceAttachment: boolean;
    constructor(file: string | Stream.Readable, filename?: string, extension?: string, forceAttachment?: boolean);
    sendHeaders(res: Express.Response): void;
}
export declare let globalCtrState: PrismControllerState;
/**************************************************
 * Class Decorators
 **************************************************/
export declare function Controller<T>(mountpoint?: string | ControllerConstructor<T>, path?: string): (f: Function) => void;
/**
 * Adds express middleware to run before mounting the controller
 * @param { Express.RequestHandler[] } middleware - Array of middleware to add.
 */
export declare function ControllerMiddleware(...middleware: Express.RequestHandler[]): (f: Function) => void;
/**
 * @param { boolean } condition - Only mounts this controller if condition is true.
 */
export declare function MountCondition(condition: boolean): (f: Function) => void;
/**
 *  Only mounts this controller if NODE_ENV is set to "development"
 */
export declare function Dev(): (f: Function) => void;
/**
 *  Attach a documentation string to the controller
 *  @param {string} docStr - The documentation string.
 */
export declare function DocController(docStr: string): (f: Function) => void;
/**
 * Generate test runner paths inside this controller
 */
export declare function TestRunner(): (f: Function) => void;
/**************************************************
 * Method Decorators
 **************************************************/
export declare function Method(method: string, path?: string): (a: any, s: string, pd: PropertyDescriptor) => void;
export declare function GET(path?: string): (a: any, s: string, pd: PropertyDescriptor) => void;
export declare function POST(path?: string): (a: any, s: string, pd: PropertyDescriptor) => void;
export declare function PUT(path?: string): (a: any, s: string, pd: PropertyDescriptor) => void;
export declare function PATCH(path?: string): (a: any, s: string, pd: PropertyDescriptor) => void;
export declare function DELETE(path?: string): (a: any, s: string, pd: PropertyDescriptor) => void;
/**
 * Adds express middleware to run before the method
 * @param { Express.RequestHandler[] } middleware - Array of middleware to add.
 */
export declare function ActionMiddleware(...middleware: Express.RequestHandler[]): (a: any, s: string, pd: PropertyDescriptor) => void;
/**
 * Flags the method as "Express Compatible" and thus will be called with parameters (req,res,next)
 */
export declare function ExpressCompatible(): (a: any, s: string, pd: PropertyDescriptor) => void;
/**
 *  Attach a documentation string to the method
 *  @param {string} docStr - The documentation string.
 */
export declare function DocAction(docStr: string): (a: any, s: string, pd: PropertyDescriptor) => void;
/**************************************************
 * Method Parameter Decorators
 **************************************************/
export declare type RequestValueContainer = "body" | "query" | "path" | "header" | "cookie";
declare type SimpleFunction = (val: any) => any;
export declare function MapParameterToRequestValue(rvc: RequestValueContainer, valueKey: string, openApiType?: string, modFunction?: SimpleFunction): (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;
export declare function FromBody(valueKey?: string, modFunction?: SimpleFunction, openApiType?: string): (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;
export declare function FromQuery(valueKey: string, modFunction?: SimpleFunction, openApiType?: string): (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;
export declare function FromPath(valueKey: string, modFunction?: SimpleFunction, openApiType?: string): (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;
export declare function FromHeader(valueKey: string, modFunction?: SimpleFunction, openApiType?: string): (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;
export declare function FromCookie(valueKey: string, modFunction?: SimpleFunction, openApiType?: string): (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;
/*********************************************************
 * Utils
 *********************************************************/
export declare function DumpInternals(): void;
/**************************************************
 * Internals
 **************************************************/
export declare type PrismMethodMountpoint = {
    path: string;
    httpMethod: string;
};
export declare type PrismExtraParametersMapping = {
    rvc: RequestValueContainer;
    valueKey: string;
    openApiType: string;
    modFunction: Function;
};
export declare class PrismMethodOpenApiResponses {
    [httpCode: string]: {
        description: string;
        type: string;
    };
}
export declare class PrismMethod {
    docString: string;
    expressCompatible: boolean;
    explicitlyDeclared: boolean;
    middleware: Express.RequestHandler[];
    methodMountpoints: PrismMethodMountpoint[];
    openApiResponses: PrismMethodOpenApiResponses;
    extraParametersMappings: PrismExtraParametersMapping[];
}
export declare type PrismMethodMap = {
    [key: string]: PrismMethod;
};
export declare type ControllerConstructor<T> = {
    new (...args: any[]): T;
};
export declare class PrismController {
    path: string;
    ctr: Function;
    docString: string;
    mountCondition: boolean;
    router: Express.Router;
    generateTestRunnerPaths: boolean;
    childController: boolean;
    explicitlyDeclared: boolean;
    methods: PrismMethodMap;
    middleware: Express.RequestHandler[];
    node: PrismControllerTreeNode;
    getOrInsertMethod(key: string): PrismMethod;
}
export declare class PrismControllerTreeNode {
    fullPath: string;
    controller: PrismController;
    children: PrismControllerTreeNode[];
    constructor(controller: PrismController);
}
export declare type PrismControllerMap = {
    [key: string]: PrismController;
};
export declare type PrismMountpoint = {
    dstCtr: Function;
    ctr: Function;
};
export declare class PrismControllerState {
    mountpoints: PrismMountpoint[];
    controllers: PrismControllerMap;
    controllersTree: PrismControllerTreeNode[];
    getController(ctr: Function): PrismController;
    getOrInsertController(ctr: Function): PrismController;
    registerMountpoint(dstCtr: any, ctr: Function): void;
}
export declare let initialized: boolean;
export declare function initialize(app: Express.Application, ...requiredDirectories: string[]): void;
export declare function initializeAtRoute(rootPath: string, app: Express.Application, ...requiredDirectories: string[]): void;
export {};
